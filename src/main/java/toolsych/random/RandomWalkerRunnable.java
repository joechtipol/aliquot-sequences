package toolsych.random;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class RandomWalkerRunnable implements Runnable {
	
	
	
	public static long MY_BIG_VALUE=Integer.MAX_VALUE;


	@Override
	public void run() {

		while(true)
		{
			double random=Math.random();
			long currentN=new BigDecimal(MY_BIG_VALUE*random).longValue();
			if(currentN%100==0)
			{
				System.out.println(currentN);

			}
				callHttpUrl(currentN);

				
						
		}
		
		
	}

	
	
	
	public void callHttpUrl(long currentN)
	{
		HttpClient client = HttpClientBuilder.create().build();

		String url = "http://ec2-52-210-236-133.eu-west-1.compute.amazonaws.com:8090/primes-portal/api/aliquot/"+currentN;

		
		String urlsecours = "http://vps471348.ovh.net:8090/primes-portal/api/aliquot/"+currentN;

		
		HttpGet request = new HttpGet(url);

		// add request header
		request.addHeader("User-Agent", "");
		HttpResponse response =null;
		try {
			 response = client.execute(request);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
		}finally {
			//client = HttpClientBuilder.create().build();
			request = new HttpGet(urlsecours);
			 try {
				response = client.execute(request);
			}  catch (IOException e) {
				// TODO Auto-generated catch block
				System.err.println(e.getMessage());
			}

		}

		//System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

	}
}
