package toolsych;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import toolsych.structure.TwinPrimeData;

public class BismlahAnalyser {

	public static final long N=5000000;
	public static final double R= Math.sqrt(N)/Math.log(N);

	public static Map<Long,String> twininfosmap=new HashMap<>();
	
	public static Map<Long,TwinPrimeData> twininfosmapdata=new HashMap<>();

	
	  public static double calculLambda(double R,long number)
	  {
		  double lambda=((1.0/2.0)*Math.pow(Math.log(R), 2));
		  long polnumber=number*(number+2);
		  
		  for(long i=2;i<R;i++)
		  {
			  if(polnumber%i==0)
			  {
				  int moebus=primeFactorsmoebus(i);
		    		lambda=lambda+moebus*((1.0/2.0)*Math.pow(Math.log(R/i),2));

			  }
		  }
	      	
		  return lambda;
	  }
	
	public static int primeFactorsmoebus(long number) {
	    int moebus=1;
	    long n = number;
	    HashMap<Long, Integer> mapfactors=new HashMap<>();
	    for (long i = 2; i <= n; i++) {
	      while (n % i == 0) {
	        if(mapfactors.get(i)==null)
	        {
	        	mapfactors.put(i, 0);
	        	moebus=moebus*(-1);
	        }
	        else
	        {
	        	return 0;
	        }
	        mapfactors.put(i,mapfactors.get(i)+1);
	        
	        n /= i;
	      }
	    }
	    return moebus;
	  }

	
	public static List<Long> primeFactors(long number) {
	    long n = number;
	    List<Long> factors = new ArrayList<Long>();
	    for (long i = 2; i <= n; i++) {
	      while (n % i == 0) {
	        factors.add(i);
	        n /= i;
	      }
	    }
	    return factors;
	  }
	
	public static Map<Long,Integer> primeFactorsMap(long number) {
	    long n = number;
	    HashMap<Long, Integer> mapfactors=new HashMap<>();
	    List<Long> factors = new ArrayList<Long>();
	    for (long  i = 2; i <= n; i++) {
	      while (n % i == 0) {
	        factors.add(i);
	        if(mapfactors.get(i)==null)
	        {
	        	mapfactors.put(i, 0);
	        }
	        mapfactors.put(i,mapfactors.get(i)+1);
	        
	        n /= i;
	      }
	    }
	    return mapfactors;
	  }
	
	public static void analyse1()
	{
	int compteur=0;	
	HashMap<Integer, Integer> mappy=new HashMap<>();
		for(long i=2;i<N;i++)
		{
			List<Long> primeFactorsi=primeFactors(i);
			List<Long> primeFactorsip2=primeFactors(i+2);
			int taille=primeFactorsi.size()+primeFactorsip2.size();
			if(mappy.get(taille)==null)
			{
				mappy.put(taille, 0);
			}
			mappy.put(taille,mappy.get(taille)+1);
			
			if(i%10000==0)
			{
			  System.out.println("i="+i+" primeFactors="+primeFactorsi+" i+2="+(i+2)+" primeFactors="+primeFactorsip2+"  compteur="+compteur);

			}
			
		}
		

		
		
		System.out.println(mappy);
		
	}

	public static void analyse2()
	{
	HashMap<Integer, Integer> mappy=new HashMap<>();
		for(long i=2;i<N;i++)
		{
			Set<Long> primeFactorsi=primeFactorsMap(i).keySet();
			Set<Long> primeFactorsip2=primeFactorsMap(i+2).keySet();
			int taille=primeFactorsi.size()+primeFactorsip2.size();
			if(mappy.get(taille)==null)
			{
				mappy.put(taille, 0);
			}
			mappy.put(taille,mappy.get(taille)+1);
			
			if(i%10000==0)
			{
			  System.out.println("i="+i+" primeFactors="+primeFactorsi+" i+2="+(i+2)+" primeFactors="+primeFactorsip2+"  mappy="+mappy);

			}
			
		}
		

		
		
		System.out.println(mappy);
		
	}
	
	
	public static void writedata() throws IOException
	{
		double sommepsi=0;
		FileOutputStream fos=new FileOutputStream("d:/twindata"+N+".txt");
		for(long i=2;i<N;i++)
		{
			
			boolean isPrime=false;
			Map<Long,Integer> primeFactorsiMap=primeFactorsMap(i);

			Set<Long> primeFactorsi=primeFactorsiMap.keySet();
			double gamma=0;
			
			if(primeFactorsi.size()==1)
			{
				long prime=primeFactorsi.iterator().next();
				int factor=primeFactorsiMap.get(prime);
				gamma=Math.log(prime);
				
				if(factor==1)
				{
					isPrime=true;
				}
				
			}
			
			sommepsi=sommepsi+gamma;
			
			String display=i+";"+primeFactorsi.toString()+";"+gamma+";"+isPrime+'\n';
			
			fos.write(display.getBytes());
			
			if(i%10000==0)
			{
			  System.out.println("i="+i+" primeFactors="+primeFactorsi+"  sommepsi="+sommepsi+"  time="+System.currentTimeMillis()/1000);

			}
			
		}
		
		fos.close();
				
	}
	
	public static void readdata() throws IOException
	{
		twininfosmap=new HashMap<>();
		double sommepsi=0;
		
		String thisLine=null;
		BufferedReader br = new BufferedReader(new FileReader("d:/twindata"+N+".txt"));
	       while ((thisLine = br.readLine()) != null) { 
	    	   String[] lineTab=thisLine.split(";");
	    	   long i=Long.valueOf(lineTab[0]);
	    	   String prmeFactorsStr=lineTab[1];
	    	   double gamma=Double.valueOf(lineTab[2]);
	    	   twininfosmap.put(i, prmeFactorsStr);
				sommepsi=sommepsi+gamma;
				if(i%10000==0)
				{
				  System.out.println("i="+i+"  sommepsi="+sommepsi+"  time="+System.currentTimeMillis()/1000);

				}
	    	   
	       }
	       
	       System.out.println(twininfosmap.size());

				
	}	

	public static void writedataextend() throws IOException
	{
		double sommepsi=0;
		
		double sommepsiapproxim=0;

		
		
		
		FileOutputStream fos=new FileOutputStream("d:/twindataextend"+N+".txt");
		for(long i=2;i<N;i++)
		{
			boolean isPrime=false;
			Map<Long,Integer> primeFactorsiMap=primeFactorsMap(i);

			Set<Long> primeFactorsi=primeFactorsiMap.keySet();
			double gamma=0;
			
			double lambdaapproxim=calculLambda(R, i);
			
			
			long prime=primeFactorsi.iterator().next();
			int factor=primeFactorsiMap.get(prime);
			if(primeFactorsi.size()==1)
			{
				gamma=Math.log(prime);
				if(factor==1)
				{
					isPrime=true;
				}
			}
			
			sommepsi=sommepsi+gamma;
			
			sommepsiapproxim=sommepsiapproxim+lambdaapproxim;
			
			String display=i+";"+primeFactorsi.toString()+";"+gamma+";"+lambdaapproxim+";"+isPrime+'\n';
			
			fos.write(display.getBytes());
			
			if(i%10000==0)
			{
			  System.out.println("i="+i+" primeFactors="+primeFactorsi+"  sommepsi="+sommepsi+"  sommepsiapproxim="+sommepsiapproxim+"  rapport="+(sommepsiapproxim/i));

			}
			
		}
		
		fos.close();
				
	}
	
	public static void readdataextend() throws IOException
	{		
		double sommepsi=0;
		double sommepsinoncarre=0;
		double sommepsiapproxim=0;

		double sommetetaapproxim=0;

		Map<Integer,Integer> mapFatal=new HashMap<>();
		
        parsedataextend();
	       for (long i=2;i<N-2;i++) { 
	    	    TwinPrimeData tpd=twininfosmapdata.get(i);
	    	    TwinPrimeData tpd2=twininfosmapdata.get(i+2);
	    	    double sommetetacurr=0;
	    	    
	    	   if(tpd.isPrime())
	    	   {
	    		   sommetetacurr=sommetetacurr+Math.log(i);
	    	   }
	    	   if(tpd2.isPrime())
	    	   {
	    		   sommetetacurr=sommetetacurr+Math.log(i+2);	   
	    	   }
	    	   
	    	   int sommefactors=(tpd.getPrimefactors().size()+tpd2.getPrimefactors().size());
	    	   
	    	   if(i%2==0)
	    	   {
	    		   sommefactors--;
	    	   }
	    	   Integer fatalcur=mapFatal.get(sommefactors);
	    	   
	    	   if(fatalcur==null)
	    	   {
	    		   fatalcur=0;
	    	   }
	    	   fatalcur++;
	    	   
	    	   mapFatal.put(sommefactors, fatalcur);

		   //if(sommefactors<5)
	    	   if(tpd.getPrimefactors().size()<2)
	    	   {
	    		   
	    

	   //  if((tpd.getPrimefactors().size()+tpd2.getPrimefactors().size())<4)
	    	    {

					sommepsi=sommepsi+tpd.getLambda();
					sommepsinoncarre=sommepsinoncarre+(tpd.getLambdaR());

					sommepsiapproxim=sommepsiapproxim+(tpd.getLambdaR()*tpd.getLambdaR());
					sommetetaapproxim=sommetetaapproxim+((tpd.getLambdaR()*tpd.getLambdaR())*sommetetacurr);
	    	    	
	    	    }
	    	   }

				if(i%10000==0)
				{
				  System.out.println("i="+i+"  sommepsinoncarre="+(sommepsinoncarre/i)+"  sommepsiapproxim="+sommepsiapproxim+"  sommetetaapproxim="+sommetetaapproxim+"  rapport="+(sommepsiapproxim/i)+"  rapport="+(sommetetaapproxim/i));

				}
	    	   
	       }
	       
	       System.out.println(twininfosmap.size());
		   System.out.println("-----  sommepsi="+sommepsi+"  sommepsiapproxim="+sommepsiapproxim+"  rapport="+(sommetetaapproxim/sommepsiapproxim));
		   System.out.println("nsurlogncarre="+N/(Math.log(N)*Math.log(N)));

		   System.out.println(mapFatal);
	}	
	
	
	public static void parsedataextend() throws IOException
	{	
		twininfosmapdata=new HashMap<>();
		String thisLine=null;
		BufferedReader br = new BufferedReader(new FileReader("d:/twindataextend"+N+".txt"));
	       while ((thisLine = br.readLine()) != null) { 
	    	   String[] lineTab=thisLine.split(";");
	    	   long i=Long.valueOf(lineTab[0]);
	    	   String prmeFactorsStr=lineTab[1];
	    	   double gamma=Double.valueOf(lineTab[2]);
	    	   boolean isPrime=Boolean.valueOf(lineTab[4]);

	    	   TwinPrimeData tpd=new TwinPrimeData();
	    	   tpd.setEntier(i);
	    	  tpd.setPrimefactors(arraysAsList(prmeFactorsStr)); 
	    	   tpd.setLambda(Double.valueOf(lineTab[2]));
	    	   tpd.setLambdaR(Double.valueOf(lineTab[3]));
	    	   tpd.setPrime(isPrime);
	    	   twininfosmapdata.put(i, tpd);

				if(i%10000==0)
				{
				  System.out.println("parsedataextend  i="+i);

				}
	    	   
	       }
	       
	       System.out.println(twininfosmapdata.size());

				
	}	

	
	public static List<Long> arraysAsList(String doubleStr)
	{
		String[] tab=doubleStr.substring(1, doubleStr.length()-1).split(",");
		List<Long> result=new ArrayList<>();
		for(int i=0;i<tab.length;i++)
		{
			result.add(Long.valueOf(tab[i].trim()));
		}
		
		return result;
		
		
	}
	
	
	public static void main(String[] args) throws IOException{
	   // analyse2();
		long timedebut=System.currentTimeMillis();
		System.out.println(timedebut);
		
		  System.out.println("N="+N+" R="+R+" log(R)="+Math.log(R)+" log(3N)="+Math.log(3*N));


		//writedata();
		//readdata();
	   //writedataextend();
		readdataextend();
		System.out.println((System.currentTimeMillis()-timedebut)/1000);
		
	}

}
