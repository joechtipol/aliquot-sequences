package toolsych;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import toolsych.structure.AliquotNumber;

public class MariaDbAliquoteur {

	public static void main(String[] args) {
		long timeDebut = System.currentTimeMillis();

		// testUseMariaDb();
		// testUseMariaDbCount();
		// testUseMariaDbInsertFactors();
		try {
			testUseMariaDbInsertCarreSprime();
			// testUseMariaDbInsertAntecedantsFactors();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		long timeFin = System.currentTimeMillis();
		long duree = timeFin - timeDebut;

		System.out.println("timing=" + duree);
	}

	public static void testUseMariaDb() {
		Connection connection = null;
		int compteur = 0;
		try {
			connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/aliquot_1?user=root&password=root");

			long offset = 10000000;
			long minValue = 2;

			for (long j = 1; j < 10; j++) {

				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from aliquot_seq_factors limit " + minValue + "," + offset);
				while (rs.next()) {
					compteur++;
					long num = rs.getLong("number");
					long sprime = rs.getLong("sprime");
					if (num % 10000 == 0)
						System.out.println("num=" + num + "  sprime=" + sprime + " compteur=" + compteur);
				}
				stmt.close();
				minValue = minValue + offset;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			try {
				connection.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void testUseMariaDbCount() {
		Connection connection = null;
		int compteur = 0;
		try {
			connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/aliquot_1?user=root&password=root");
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("select count(*) as mycount from aliquot_seq_factors");
			while (rs.next()) {
				compteur++;
				long mycount = rs.getLong("mycount");
				System.out.println("mycount=" + mycount);
			}
			stmt.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			try {
				connection.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void testUseMariaDbInsertFactors() {
		Connection connection = null;
		int compteur = 0;
		try {
			connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/aliquot_1?user=root&password=root");
			Statement stmt = connection.createStatement();

			for (long i = 2; i < 1000000; i++) {
				String factors = AliquotSystemConj.primeFactorsMap(i).toString();
				String insertsql = "INSERT INTO test_factors (number, factors) VALUES (" + i + ",'" + factors + "')";
				System.out.println(i);
				stmt.executeQuery(insertsql);

			}

			stmt.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			try {
				connection.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void testUseMariaDbInsertAntecedantsFactors() throws SQLException {
		Connection connection = DriverManager
				.getConnection("jdbc:mariadb://localhost:3306/aliquot_1?user=root&password=root");
		try {

			BufferedReader br = new BufferedReader(new FileReader("c:/database/aliquot/antecedents_1e5"));
			String thisLine = null;

			while ((thisLine = br.readLine()) != null) {
				String[] argsparsed = thisLine.split("],");
				long MAX_COUNT = argsparsed.length;
				for (int i = 0; i < argsparsed.length; i++) {

					String argsparsedstr = argsparsed[i];
					argsparsedstr = argsparsedstr.replace("[", "");
					argsparsedstr = argsparsedstr.replace("]", "");
					argsparsedstr = argsparsedstr.trim();

					String[] paramsparsed = argsparsedstr.split(",");
					ArrayList<Long> listantecedants = new ArrayList<>();
					long currentn = Long.parseLong(paramsparsed[0].trim());
					if (currentn > 50345) {
						for (int j = 1; j < paramsparsed.length; j++) {
							String insertsql = null;
							try {

								Statement stmt = connection.createStatement();
								long currentAntec = Long.parseLong(paramsparsed[j].trim());

								if (currentAntec > 99999999) {
									AliquotNumber an = AliquotSystemConj.sigmaprime2(currentAntec);
									Map<Long, Long> factorsMap = an.getPrimeFactors();
									String factorsStr = factorsMap.toString();
									insertsql = "INSERT INTO aliquot_seq_factors (number, factors,sprime,sprime2) VALUES ("
											+ currentAntec + ",'" + factorsStr + "'," + an.getSigma() + ",0)";
									System.out.println(insertsql);
									stmt.executeQuery(insertsql);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

					}
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void testUseMariaDbInsertCarreSprime() throws SQLException {
		Connection connection = DriverManager
				.getConnection("jdbc:mariadb://localhost:3306/aliquot_1?user=root&password=root");
		try {
//58094
			for (long K = 648810; K < 1000000; K++) {
				long j = 2 * K + 1;
				double sommeDiv = 1.0;
				BigDecimal sommeDivCarre = new BigDecimal(1.0);
				Map<Long, Long> factorsMap = AliquotSystemConj.primeFactorsMap(j);
				Map<Long, Long> factorsMapCarre = new HashMap<>();

				Iterator<Long> e = factorsMap.keySet().iterator();

				while (e.hasNext()) {
					long currentP = e.next();
					long power = factorsMap.get(currentP);
					factorsMapCarre.put(currentP, 2 * power);
					sommeDiv = sommeDiv * AliquotSystemConj.getSommeDiviserurPrimeProper(currentP, power);

					sommeDivCarre = sommeDivCarre.multiply(getSommeDiviserurPrimeProper(currentP, 2 * power));

				}
				sommeDiv = sommeDiv - j;

				BigDecimal bd = new BigDecimal(j).pow(2);
				sommeDivCarre = sommeDivCarre.subtract(bd);

				String insertsql = "";
				try {

					Statement stmt = connection.createStatement();
                    if(sommeDivCarre.longValueExact()>50000)
                    {
					String factorsStr = factorsMapCarre.toString();
					insertsql = "INSERT INTO aliquot_seq_factors (number, factors,sprime,sprime2) VALUES ("
							+ bd.toString() + ",'" + factorsStr + "'," + sommeDivCarre.toString() + ",0)";
					System.out.println(insertsql);
					stmt.executeQuery(insertsql);
                    }

				} catch (Exception f) {
					f.printStackTrace();
				}

				if (K % 1000 == 0)
					System.out.println("j=" + j + "  sommeDiv=" + sommeDiv + "  sommeDivCarre=" + sommeDivCarre);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static BigDecimal getSommeDiviserurPrimeProper(long prime, long power) {
		BigDecimal resultat = BigDecimal.ZERO;
		for (int i = 0; i < power + 1; i++) {
			resultat = resultat.add((new BigDecimal(prime)).pow(i));
		}

		return resultat;
	}

}
