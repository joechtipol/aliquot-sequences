package toolsych;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.MongoCredential;
import com.mongodb.MongoClientOptions;

public class MongoDbDatabase {
	
	public static Map<Integer,Integer> primeFactorsMap(int number) {
	    int n = number;
	    HashMap<Integer, Integer> mapfactors=new HashMap<>();
	    List<Integer> factors = new ArrayList<Integer>();
	    for (int i = 2; i <= n/i; i++) {
	      while (n % i == 0) {
	        factors.add(i);
	        if(mapfactors.get(i)==null)
	        {
	        	mapfactors.put(i, 0);
	        }
	        mapfactors.put(i,mapfactors.get(i)+1);
	        
	        n /= i;
	      }
	    }
	    return mapfactors;
	  }
	
	

	public static void main(String[] args) throws IOException {
		MongoClient mongoClient = new MongoClient( "192.168.0.14" , 27017 );
		MongoDatabase database = mongoClient.getDatabase("openaliquotdb");
		MongoCollection<Document> collection = database.getCollection("PRIME_FACTORS");

		
		int N=10000000;
		//FileOutputStream fos=new FileOutputStream("d:/insertdb.sql");
		for(int i=0;i<N;i++)
		{
			double Lambdan=0.0;
			int moebus=1;
			Map<Integer, Integer> factorsMap=primeFactorsMap(i);
			if(factorsMap.keySet().size()==1)
			{
				Lambdan=Math.log(factorsMap.keySet().iterator().next());
			}
			
			Iterator<Integer> factors=factorsMap.keySet().iterator();
			while(factors.hasNext())
			{
				if(factorsMap.get(factors.next())>1)
				{
					moebus=0;
				}
				else
				{
					moebus=moebus*(-1);
				}
				
			}
			
			Document document = new Document("id",String.valueOf(i)).append("un", String.valueOf(moebus)).append("Lambdan", String.valueOf(Lambdan)).append("mapfactors",factorsMap.toString() );
			//System.out.println("i="+i+"  moebus="+moebus+"  Lambdan="+Lambdan+"  factorsMap= "+factorsMap);
			//System.out.println("INSERT INTO PRIMES_FACTORS VALUES ("+i+",'"+factorsMap.toString()+"',"+moebus+","+Lambdan+");");
			//fos.write(("INSERT INTO PRIMES_FACTORS VALUES ("+i+",'"+factorsMap.toString()+"',"+moebus+","+Lambdan+");"+'\n').getBytes());
			collection.insertOne(document);

			if(i%10000==0)
			{
				System.out.println("INSERT INTO PRIMES_FACTORS VALUES ("+i+",'"+factorsMap.toString()+"',"+moebus+","+Lambdan+");");

			}
			
			//System.out.println("db.mynumbersall.insert({id:\""+i+"\", un:\""+moebus+"\",lambdan:\""+Lambdan+"\",mapfactors:\""+factorsMap.toString()+"\"})") ;
			//fos.write(("db.mynumbersall.insert({id:\""+i+"\", un:\""+moebus+"\",lambdan:\""+Lambdan+"\",mapfactors:\""+factorsMap.toString()+"\"})"+'\n').getBytes()) ;

		}
		mongoClient.close();
		//fos.close();

	}

}
