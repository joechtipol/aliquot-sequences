package toolsych;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

public class ColossalyCalculRunnable implements Runnable {

	List<BigDecimal> epsilons;
	
	
	StringBuffer sb=new StringBuffer();

	public StringBuffer getSb() {
		return sb;
	}

	public void setSb(StringBuffer sb) {
		this.sb = sb;
	}

	public ColossalyCalculRunnable(List<BigDecimal> lesEpsilons) {
		epsilons=new ArrayList<>();
		epsilons.addAll(lesEpsilons);
	}

	@Override
	public void run() {
		double sommepsilon=0;
		for(int i=0;i<epsilons.size();i++)
		{
			BigDecimal currentEpsilo=epsilons.get(epsilons.size()-i-1);
			//if(currentEpsilo.compareTo(epsilonMax)<0)
			{
				sommepsilon=sommepsilon+currentEpsilo.doubleValue();
				List<Long> factorsCA=ColossalyCalcul1.calculerN0List(currentEpsilo);
				double logN=ColossalyCalcul1.calculLogN(factorsCA);
				BigDecimal abundance=new BigDecimal(Math.exp(ColossalyCalcul1.calculAbundance(factorsCA)));
				
				BigDecimal extremAbundance=abundance.divide(new BigDecimal(Math.log(logN)),MathContext.DECIMAL128);
				
				abundance=abundance.setScale(8, BigDecimal.ROUND_UP);

				String strFile=""+(i+1)+";"+currentEpsilo+";"+factorsCA+";"+logN+";"+abundance+";"+extremAbundance+'\n';
				sb.append(strFile.getBytes());
				System.out.println(""+(i+1)+"  "+currentEpsilo+"  "+factorsCA+"  "+logN+"  "+abundance+";"+extremAbundance);
	
			}
		}
		
	}
	
}
