FROM ubuntu:16.04

# Install dependencies
RUN apt-get update
RUN apt-get -y install default-jdk
RUN apt-get -y install maven
RUN apt-get -y install git-core


RUN             cd /home && \
                git clone https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/aliquot-sequences && \
                cd aliquot-sequences && \
                mvn clean package
WORKDIR         /home/aliquot-sequences
ENTRYPOINT      ["nohup java toolsych.DbFactorsRunMain > dblogs.txt &"]
