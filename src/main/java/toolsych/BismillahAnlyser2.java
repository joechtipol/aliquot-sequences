package toolsych;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import toolsych.structure.TwinPrimeData;

public class BismillahAnlyser2 {
	
	public static final long N=1500000;

	
	public static Map<Long,TwinPrimeData> twininfosmapdata=new HashMap<>();


	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	
	public static void parsedataextend(long otherN) throws IOException
	{	
		
		if(otherN<1)
		{
			otherN=N;
		}
		twininfosmapdata=new HashMap<>();
		String thisLine=null;
		BufferedReader br = new BufferedReader(new FileReader("d:/twindataextend"+otherN+".txt"));
	       while ((thisLine = br.readLine()) != null) { 
	    	   String[] lineTab=thisLine.split(";");
	    	   long i=Long.valueOf(lineTab[0]);
	    	   String prmeFactorsStr=lineTab[1];
	    	   double gamma=Double.valueOf(lineTab[2]);
	    	   boolean isPrime=Boolean.valueOf(lineTab[4]);

	    	   TwinPrimeData tpd=new TwinPrimeData();
	    	   tpd.setEntier(i);
	    	  tpd.setPrimefactors(arraysAsList(prmeFactorsStr)); 
	    	   tpd.setLambda(Double.valueOf(lineTab[2]));
	    	   tpd.setLambdaR(Double.valueOf(lineTab[3]));
	    	   tpd.setPrime(isPrime);
	    	   twininfosmapdata.put(i, tpd);

				if(i%10000==0)
				{
				  System.out.println("parsedataextend  i="+i);

				}
	    	   
	       }
	       
	       System.out.println(twininfosmapdata.size());

				
	}
	
	public static List<Long> arraysAsList(String doubleStr)
	{
		String[] tab=doubleStr.substring(1, doubleStr.length()-1).split(",");
		List<Long> result=new ArrayList<>();
		for(int i=0;i<tab.length;i++)
		{
			result.add(Long.valueOf(tab[i].trim()));
		}
		
		return result;
		
		
	}
}
