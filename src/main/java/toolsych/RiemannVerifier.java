package toolsych;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RiemannVerifier {
   
	public static final long NMAX=1000000;
	public static List<String> linesList=null;
	
	public static List<Long> primes=new ArrayList<>();
	
	public static String  MODE_OS="WIN";

	
	public static void main(String[] args) throws NumberFormatException, IOException {
		if(args.length>0)
		{
			MODE_OS=args[0];
			
			System.out.println("NMAW recharged="+MODE_OS);
		}
		
		String path="c:/data/epsilons_ca_list.txt";
		if(!MODE_OS.equals("WIN"))
		{
			path="/home/datanumber/epsilons_ca_list.txt";
		}
		
		
		FileInputStream fis=new FileInputStream(path);
        long compteurMu=0;
        long compteur=0;
        
        List<BigDecimal> oneplusMuList=new ArrayList<>();
		
		InputStreamReader isr=new InputStreamReader(fis);
		linesList=new ArrayList<>();
		List<Double> myLst=new ArrayList<>();
		String line=null;
		BufferedReader br=new BufferedReader(isr);
        BigDecimal bigRapport=BigDecimal.ZERO;
		
        BigDecimal minrapport=new BigDecimal(Double.MAX_VALUE);
        
		while((line=br.readLine())!=null)
		{
			linesList.add(line);
			compteur++;
			String[] tab=line.trim().split(";");
			int indexCl=Integer.parseInt(tab[0]);
			BigDecimal epsilon=new BigDecimal(tab[1]);
			String factors=tab[2];
			BigDecimal logN=new BigDecimal(tab[3]);
			BigDecimal log2N=new BigDecimal(Math.log(logN.doubleValue()));
			BigDecimal abundance=new BigDecimal(tab[4]);
			BigDecimal extremAbundance=new BigDecimal(tab[5]);
            BigDecimal oneplusmu=epsilon.multiply(logN).multiply(log2N);
            if(oneplusmu.compareTo(BigDecimal.ONE)>0)
            {
            	compteurMu++;
            	oneplusMuList.add(oneplusmu);
            	if(oneplusmu.compareTo(minrapport)<0)
            	{
            		minrapport=oneplusmu;
            	}
            	System.out.println(compteurMu+";"+minrapport.doubleValue()+";"+oneplusmu.doubleValue()+";"+line);
            }
		}
		
		Collections.sort(oneplusMuList);
		
		
		for(int j=oneplusMuList.size()-1;j>=0;j--)
		{
			double mu=oneplusMuList.get(j).doubleValue();
			if(mu<1.01)
			runSystemForMu(oneplusMuList.get(j));
			//System.out.println();
		}
		
		
	}
	public static void mainVerif(String[] args) throws NumberFormatException, IOException {
		FileInputStream fis=new FileInputStream("c:/data/epsilons_ca_list.txt");
      
		InputStreamReader isr=new InputStreamReader(fis);
		linesList=new ArrayList<>();
		List<Double> myLst=new ArrayList<>();
		String line=null;
		BufferedReader br=new BufferedReader(isr);
        int compteur=0;
        BigDecimal bigRapport=BigDecimal.ZERO;
		
		while((line=br.readLine())!=null)
		{
			linesList.add(line);
		}
		
		List<Double> listMus=readMuVas();
		for(int j=0;j<listMus.size();j++)
		{
			double mu=listMus.get(j);
			if(mu<1.01)
			runSystemForMu(new BigDecimal(mu));
		}
		
	}
	
	
	public static void mainverif(String[] args) {
        initPrimes(NMAX);
        System.out.println(primes.size());
		validateSum(11);
	}
	
	public static void initPrimes(long XMAX)
	{
		primes=new ArrayList<>();
		
		for(long i=2;i<=XMAX;i++)
		{
			long sprime=AliquotSystemConj.sigmaprime(i);
			if(sprime==1)
			{
				primes.add(i);
			}

		}		
	}
	
	
	public static long validateSum(long p)
	{
		
		BigDecimal sommeHaute=new BigDecimal(0);
		BigDecimal sommeBasse=new BigDecimal(0);
		BigDecimal rapportSums=BigDecimal.ZERO;
		int indexp=primes.indexOf(p);
        long psuivant=primes.get(indexp+1);
		BigDecimal epsilonp=new BigDecimal(Math.log(1.0+(1.0/psuivant))).divide(new BigDecimal(Math.log(psuivant)),MathContext.DECIMAL128);
		for(int i=(indexp+1);i<primes.size();i++)
		{
			long q=primes.get(i);
			sommeHaute=sommeHaute.add(new BigDecimal(Math.log(1.0+(1.0/q))));
			sommeBasse=sommeBasse.add(new BigDecimal(Math.log(q)));
			
			rapportSums=sommeHaute.divide(sommeBasse,MathContext.DECIMAL128);
			
			System.out.println("pour p="+p+"  find q="+q+"  epsilonp="+epsilonp+"  rapport="+rapportSums);

			
			if(rapportSums.compareTo(epsilonp)>0)
			{
				System.out.println("pour p="+p+"  find q="+q+"  epsilonp="+epsilonp+"  rapport="+rapportSums);
				break;
				
			}
			
			
		}	
		
		return -1;
	}
	
	public static List<Double> readMuVas() throws IOException
	{		
		
		FileInputStream fis=new FileInputStream("c:/data/SAN_modified_mu_ca_1000000.txt");

		InputStreamReader isr=new InputStreamReader(fis);
		
		List<Double> myLst=new ArrayList<>();
		String line=null;
		BufferedReader br=new BufferedReader(isr);

		while((line=br.readLine())!=null)
		{
			String[] tab=line.trim().split(";");
			myLst.add(Double.parseDouble(tab[1]));
		}
		
		return myLst;
	}
	
	
	public static void runSystemForMu(BigDecimal mu) throws IOException
	{
         BigDecimal bigRapport=BigDecimal.ZERO;
         long index=0;
         int compteur=0;
         double muminusone=mu.doubleValue()-1.0;
         double logNmax=0;;

         String factorsMax=null;
         
         for(int i=1;i<linesList.size();i++)
         {
        	 String line=linesList.get(i);

             String[] tab=line.trim().split(";");
    			int indexCl=Integer.parseInt(tab[0]);
    			BigDecimal epsilon=new BigDecimal(tab[1]);
    			String factors=tab[2];
    			BigDecimal logN=new BigDecimal(tab[3]);
    			BigDecimal abundance=new BigDecimal(tab[4]);
    			BigDecimal extremAbundance=new BigDecimal(tab[5]);
    			BigDecimal log2nmu=new BigDecimal(Math.pow(Math.log(logN.doubleValue()), muminusone));
    			BigDecimal extremCurrentAbundance=extremAbundance.divide(log2nmu,MathContext.DECIMAL128);
    			if(extremCurrentAbundance.compareTo(bigRapport)>0 && indexCl>8)
    			{
    				bigRapport=extremCurrentAbundance;
    				index=indexCl;
    				factorsMax=factors;
    				logNmax=logN.doubleValue();
    			}
         }
      
			System.out.println(mu.doubleValue()+";"+bigRapport+";"+index+";"+factorsMax+";"+logNmax);

		
		
		
		
		
	}
	
	
	

}
