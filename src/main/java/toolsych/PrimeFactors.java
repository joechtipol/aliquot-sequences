package toolsych;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrimeFactors {
	
	public static Map<Long, Integer> moebiusmap=new HashMap<>();
	
	
  public static List<Long> primeFactors(long number) {
    long n = number;
    List<Long> factors = new ArrayList<Long>();
    for (long i = 2; i <= n; i++) {
      while (n % i == 0) {
        factors.add(i);
        n /= i;
      }
    }
    return factors;
  }
  
  public static Map<Integer,Integer> primeFactorsMap(int number) {
	    int n = number;
	    HashMap<Integer, Integer> mapfactors=new HashMap<>();
	    List<Integer> factors = new ArrayList<Integer>();
	    for (int i = 2; i <= n; i++) {
	      while (n % i == 0) {
	        factors.add(i);
	        if(mapfactors.get(i)==null)
	        {
	        	mapfactors.put(i, 0);
	        }
	        mapfactors.put(i,mapfactors.get(i)+1);
	        
	        n /= i;
	      }
	    }
	    return mapfactors;
	  }
  
  public static int getMobius(long number)
  {
	  if(moebiusmap.get(number)==null)
	  {
		  moebiusmap.put(number, primeFactorsmoebus(number));
	  }
	  return moebiusmap.get(number);
	  
  }
  
  public static int primeFactorsmoebus(long number) {
	    int moebus=1;
	    long n = number;
	    HashMap<Long, Integer> mapfactors=new HashMap<>();
	    for (long i = 2; i <= n; i++) {
	      while (n % i == 0) {
	        if(mapfactors.get(i)==null)
	        {
	        	mapfactors.put(i, 0);
	        	moebus=moebus*(-1);
	        }
	        else
	        {
	        	return 0;
	        }
	        mapfactors.put(i,mapfactors.get(i)+1);
	        
	        n /= i;
	      }
	    }
	    
	    if(mapfactors.keySet().size()>2)
	    {
	    	return 0;
	    }
	    return moebus;
	  }
  
  
  public static double calculLambdarestricti(double R,int number)
  {
	  double lambda=(0.5*Math.log(R)*Math.log(R));
	  
	  Map<Integer,Integer> factorsmap=primeFactorsMap(number);
	  
	    for (Integer curprime : factorsmap.keySet()) {
	    	if(curprime<R)
	    	{
	    		lambda=lambda+(0.5*Math.log(R/curprime)*Math.log(R/curprime));
	    	}
	    }

	  
	  return lambda;
  }
  
  public static double calculLambda(double R,long number)
  {
	  double lambda=((1.0/6.0)*Math.pow(Math.log(R), 3));
	  long polnumber=number*(number+2);
	  
	  for(long i=2;i<R&&i<=polnumber;i++)
	  {
		  if(polnumber%i==0)
		  {
			  int moebus=getMobius(i);
	    		lambda=lambda+moebus*((1.0/6.0)*Math.pow(Math.log(R/i),3));

		  }
	  }
      	 

	  
	  return lambda;
  }
  
  public static double calculLambdadouble(double R,long number)
  {
	  double lambda=(0.5*Math.log(R)*Math.log(R));
	  long polnumber=number*(number+2);
	  
	  for(long i=2;i<R&&i<=polnumber;i++)
	  {
		  if(polnumber%i==0)
		  {
			  int moebus=getMobius(i);
	    		lambda=lambda+moebus*(0.5*Math.log(R/i)*Math.log(R/i));

		  }
	  }
      	 

	  
	  return lambda;
  }

  public static void main(String[] args) {
	
	  int N=1000000;
	  int compteur=0;
	  double somme=0;
	  double sommeTeTa=0;

	  //double R=N/(Math.log(N)*Math.log(N));
	  //double R=Math.sqrt(N)/Math.sqrt(Math.log(N));

	  double R=Math.sqrt(N);
	  
	  System.out.println("N="+N+" R="+R+" log(R)="+Math.log(R)+" log(N)="+Math.log(N));


	  for(int i=N;i<2*N;i++)
	  {
		  double lamdbdan=calculLambda(R, i);
		  //if(lamdbdan>=0)
		  {
		   compteur++;
			 // System.out.println("i="+i+" somme="+somme+"  sommeTeTa="+sommeTeTa+"  rapport="+(sommeTeTa/somme)+"  compteur="+compteur);

		  somme=somme+(Math.pow(lamdbdan, 8));
		  double sommeTetacurr=0;
		  if(primeFactors(i).size()==1)
		  {
			  sommeTetacurr=sommeTetacurr+Math.log(i);
		  }
		  if(primeFactors(i+2).size()==1)
		  {
			  sommeTetacurr=sommeTetacurr+Math.log(i+2);
		  }
		  sommeTeTa=sommeTeTa+(sommeTetacurr*(Math.pow(lamdbdan, 8)));

		  }
		  

		  if(i%100000==0)
		  {
			  System.out.println("i="+i+" somme="+somme+"  sommeTeTa="+sommeTeTa+"  rapport="+(sommeTeTa/somme)+"  compteur="+compteur);
		  }
	  }
	  
	  System.out.println(" somme="+somme+"  sommeTeTa="+sommeTeTa+"  rapport="+(sommeTeTa/somme)+"  compteur="+compteur);
	  
	  /*
    System.out.println("Primefactors of 44");
    for (Integer integer : primeFactorsMap(44).keySet()) {
      System.out.println(integer);
    }
    System.out.println("Primefactors of 3");
    for (Integer integer : primeFactors(3)) {
      System.out.println(integer);
    }
    System.out.println("Primefactors of 32");
    for (Integer integer : primeFactorsMap(32).keySet()) {
      System.out.println(integer);
    }*/
  }
  
  public static BigInteger sum_of_divisors(BigInteger number)
	{
		BigInteger sum = BigInteger.ONE;
		BigInteger n=number;

		for(BigInteger k = new BigInteger("2");k.multiply(k).compareTo(n)<=0; k=k.add(BigInteger.ONE))
		{
			BigInteger p =BigInteger.ONE;
			while(n.mod(k).equals(BigInteger.ZERO))
			{
				p = p.multiply(k).add(BigInteger.ONE);
				n=n.divide(k);
			}

			sum=sum.multiply(p);
		}

		if(n.compareTo(BigInteger.ONE)>0)
			sum =sum.multiply(n.add(BigInteger.ONE));

		return sum.subtract(number);
	}
  
  public static BigInteger sigma(BigInteger number)
	{
		BigInteger sum = BigInteger.ONE;
		BigInteger n=number;

		for(BigInteger k = new BigInteger("2");k.multiply(k).compareTo(n)<=0; k=k.add(BigInteger.ONE))
		{
			BigInteger p =BigInteger.ONE;
			while(n.mod(k).equals(BigInteger.ZERO))
			{
				p = p.multiply(k).add(BigInteger.ONE);
				n=n.divide(k);
			}

			sum=sum.multiply(p);
		}

		if(n.compareTo(BigInteger.ONE)>0)
			sum =sum.multiply(n.add(BigInteger.ONE));

		return sum;
	}
  
  
  public static Map<BigInteger, Long> primefactormap(BigInteger number)
	{
		HashMap<BigInteger, Long> facteursmap=new HashMap<>();
		BigInteger n=new BigInteger(number.toString());

		for(BigInteger k = new BigInteger("2");k.multiply(k).compareTo(n)<=0; k=k.add(BigInteger.ONE))
		{
			BigInteger p =BigInteger.ONE;
			while(n.mod(k).equals(BigInteger.ZERO))
			{
			
			if(facteursmap.get(k)==null)
			{
				facteursmap.put(k,new Long(1));
			}
			else
			{
				facteursmap.put(k,new Long(facteursmap.get(k)+1));
			}
				p = p.multiply(k).add(BigInteger.ONE);
				n=n.divide(k);
			}

		}

		if(n.compareTo(BigInteger.ONE)>0)
			facteursmap.put(n, new Long(1));

		return facteursmap;
	}
  
}