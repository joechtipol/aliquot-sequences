package toolsych;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import toolsych.structure.AliquotNumber;

public class AliquotSystemConj {
	
	
	
	
	public static final long N_MAX=100000000;
	public static final long primeref=17;
	
	
	public static Map<Long, Long> sigmapvalues=new HashMap<>();
	
	public static void main(String[] args) throws IOException
	{
		long compteur=0;
		double somme=0;
		double density=1.0/30;
		BufferedReader br = new BufferedReader(new FileReader("c:/database/aliquot/anteced_fn_1e5.csv"));
        String thisLine=null;

	 while ((thisLine = br.readLine()) != null) {
		 String[] currentLineTab=thisLine.trim().split(";");
		 long i=Long.parseLong(currentLineTab[0].trim());
		 double fn=Double.parseDouble(currentLineTab[1].trim());
		 double randomi=Math.random();
	     if(randomi<density)
		 //if(i%3==1 && i<20000)
		 {
			 somme=somme+fn;
			 System.out.println("i="+i+" f(n)="+fn+"  somme="+somme/i+" compteur="+compteur++);

		 }
		    /*
			double randomi=Math.random();
			if(randomi<density)
			{
				long sprime=sigmaprime(i);
				if(sprime<Math.sqrt(N_MAX))
				{
					somme=somme+(1.0*sprime/i);
					compteur++;
					System.out.println("i="+i+"  sprime="+sprime+"  somme="+somme+" compteur="+compteur);
					
			   }
			}*/
		}
		
		
	}
	
	
	public static void mainproba(String[] args) throws IOException
	{
		long compteur=0;
		double somme=0;
		double density=1.0/6;
		for(long i=2;i<N_MAX;i++)
		{
			double randomi=Math.random();
			if(randomi<density)
			{
				long sprime=sigmaprime(i);
				if(sprime<Math.sqrt(N_MAX))
				{
					somme=somme+(1.0*sprime/i);
					compteur++;
					System.out.println("i="+i+"  sprime="+sprime+"  somme="+somme+" compteur="+compteur);
					
			   }
			}
		}
		
		
	}
	
	
	public static void mainvalitop(String[] args) throws IOException
	{
		String thisLine=null;
   int compteur=0;
		BufferedReader br = new BufferedReader(new FileReader("c:/database/aliquot/valitttop_5e7"));
		double somme=0;
		long maxAntecedantsPairs=0;
		long maxAntecedantsPairsCount=0;

	       while ((thisLine = br.readLine()) != null) { 
	    	  // System.out.println(thisLine);
    		   String argsparsedstr=thisLine.replace("{", "");
    		   argsparsedstr=argsparsedstr.replace("}", "");
    		   argsparsedstr=argsparsedstr.trim();
	    	   String[] argsparsed=argsparsedstr.split(",");
	    	   for(int i=0;i<argsparsed.length;i++)
	    	   {

	    		   if(argsparsed[i].trim().length()>0)
	    		   {
		    		   compteur++;
		    		   long nbAntecedants=Long.parseLong(argsparsed[i].trim());
		    		   if(compteur%2==0 )
		    		   {
		    			   if(nbAntecedants>0)
		    			   {
				    		   somme=somme+nbAntecedants;

		    			   }

		    			   if(nbAntecedants>maxAntecedantsPairs)
		    			   {
			    			   maxAntecedantsPairs=nbAntecedants;

		    				   maxAntecedantsPairsCount=compteur;
		    			   }

		    		   }
                       if(compteur<1000)
		    		   System.out.println("i="+compteur+" nbAntecedants="+ nbAntecedants+"  somme="+somme+"  reapporgt="+(somme/compteur)+"  maxAntecedantsPairs="+maxAntecedantsPairs);

	    		   }
	    	   }

	       }
	}
	
	
	public static void mainprincipal(String[] args) throws IOException
	{
		double somme=0;
		long compteur=0;
		Map<Long,List<Long>> twininfosmapdata=new HashMap<>();
		String thisLine=null;
		double maxAntecedantsPairs=0;
		long maxAntecedantsPairsCount=0;
		//BufferedReader br = new BufferedReader(new FileReader("d:/antecedentsbruteee_10000"));
        FileOutputStream fos=new FileOutputStream("d:/anteced_fn_1e5");
		BufferedReader br = new BufferedReader(new FileReader("d:/antecedents_1e5"));
		ArrayList<Long> valuesgu=new ArrayList<>();
		ArrayList<Double> valuesguinterm=new ArrayList<>();

		for(int i=0;i<200;i++)
		{
			valuesgu.add((long)0);
		}
	       while ((thisLine = br.readLine()) != null) { 
	    	   String[] argsparsed=thisLine.split("],");
	    	   long MAX_COUNT=argsparsed.length;
	    	   for(int i=0;i<argsparsed.length;i++)
	    	   {
	    		   
	    		   String argsparsedstr=argsparsed[i];
	    		   argsparsedstr=argsparsedstr.replace("[", "");
	    		   argsparsedstr=argsparsedstr.replace("]", "");
	    		   argsparsedstr=argsparsedstr.trim();
	    		   
	    		   String[] paramsparsed=argsparsedstr.split(",");
	    		   ArrayList<Long> listantecedants=new ArrayList<>();	
	    		   long currentn=Long.parseLong(paramsparsed[0].trim());
	    		   
	    		   for(int j=1;j<paramsparsed.length;j++)
	    		   {
	    			   listantecedants.add(Long.parseLong(paramsparsed[j].trim()));
	    		   }
	    		   
	    		   double sommeinversantec=getSommeInvlist(listantecedants);
	    		   double gcurrentn=(sommeinversantec*currentn);
	    		   if(gcurrentn/Math.log(currentn)>maxAntecedantsPairs)
	    		   {
	    			   maxAntecedantsPairs=gcurrentn/Math.log(currentn);
	    			   maxAntecedantsPairsCount=currentn;
	    		   }
	    		   //if(currentn<15001)
	    		   {
		    		   somme=somme+gcurrentn;
		    		   int curvalrap=(int) (gcurrentn/0.1);
		    		   
		    		   valuesgu.set(curvalrap, valuesgu.get(curvalrap)+1);
		    		   //if(gcurrentn>3.5)
		    		   {
		    			   compteur++;
			    		   System.out.println("n="+currentn+"  g(n)="+gcurrentn+"  somme="+somme);
                           String lineresult=currentn+";"+gcurrentn+'\n';
                           fos.write(lineresult.getBytes());
		    		   }
	    		   }

	    	   }
	    	   for(int i=0;i<valuesgu.size();i++)
	    	   {
	    		   long initvalue=0;
	    		   for(int j=0;j<=i;j++)
	    		   {
	    			   initvalue=initvalue+valuesgu.get(j);
	    		   }
	    		   valuesguinterm.add(1.0*initvalue/MAX_COUNT);
	    	   }
	    	   
	    	   System.out.println("maxAntecedantsPairs="+maxAntecedantsPairs+"  maxAntecedantsPairsCount="+maxAntecedantsPairsCount);

	    	   
	    	   System.out.println("valuesgu="+valuesgu);

	    	   System.out.println("valuesguinterm="+valuesguinterm);
	       }
	       br.close();
	       fos.close();
	}
	

	public static void main25072017(String[] args) throws IOException {
		long timeDebut=System.currentTimeMillis();
		int compteur=0;
		double somme=0;
		double seuilNmax=Math.sqrt(N_MAX);
		for(long i=2;i<N_MAX;i++)
		{
			//long sigmai=sigmaprime(i);
			long sigmai=getSommeDiviserurProper(i);
			if(sigmai<=N_MAX)
			{
				compteur++;
				somme=somme+((double)sigmai/i);
				
			}
			if(i%10000==0){
				System.out.println("i="+i+"  somme="+somme+"  compteur="+compteur);		
			}
		}
		

		long timeFin=System.currentTimeMillis();
		long duree=timeFin-timeDebut;
		System.out.println("duree="+duree);
		
	}
	
	public static void main0508(String[] args) throws IOException {
		long timeDebut=System.currentTimeMillis();
		int compteur=0;
		double somme=0;
		Map<Long,Long> primeStat=new HashMap<>();
		FileOutputStream fos=new FileOutputStream("c:/database/aliquot/"+N_MAX+"_sigmas.txt");
		for(long i=2;i<N_MAX;i++)
		{
			AliquotNumber ani=sigmaprime3(i);
			long sigmai=ani.getSigma();

			//long sigmai=sigmaprime(i);
			//if(sigmai<=N_MAX)
			{
				compteur++;
				somme=somme+((double)sigmai/i);
				
				Map<Long,Long> primeFactorization = ani.getPrimeFactors();
				
				
				String resultLine=i+";"+sigmai+";"+primeFactorization.toString()+'\n';
				fos.write(resultLine.getBytes());
				
				
				/*
				for (long p:primeFactorization.keySet()) {
					if(primeStat.get(p)==null)
					{
						primeStat.put(p, new Long(0));
					}
					long currenind=primeStat.get(p);
					primeStat.put(p, currenind+1);
				}
				*/
				
			}
			if(i%10000==0){
				System.out.println("i="+i+"  somme="+somme+"  compteur="+1);		
			}
		}
		fos.close();
		long timeFin=System.currentTimeMillis();
		long duree=timeFin-timeDebut;
		System.out.println("timing="+duree+"  somme/compteur="+(somme/compteur)+"  somme/N_MAX="+somme/N_MAX);
		System.out.println("primeStat="+primeStat);
	}

	public static void mainantec(String[] args) {
		int compteur=0;
		double somme=0;
		double u=3;
		for(long k=2;k<10000;k++)
		{
			long sigmak=sigmaprimecache(k);
			if(sigmak>1)
			{
				ArrayList<Long> antec=getAntecedants(sigmak);
				double antecsominv=getSommeInvlist(antec);
				if(antecsominv*sigmak>u)
				{
					System.out.println("n="+k+": s(n)="+sigmak+" (somme(s(n)/ni))="+antecsominv*sigmak+"  antecedants de s(n)="+antec+" compteur="+compteur++);

				}
				somme=somme+(antecsominv*sigmak);
				
				//System.out.println("n="+k+": s(n)="+sigmak+" (somme(s(n)/ni))="+antecsominv*sigmak+"  antecedants de s(n)="+antec);

				//System.out.println("k="+k+":"+sigmak+" antecsominv="+antecsominv*k+" rapport="+somme/k+"  factors="+primeFactorsMap(k)+"  map="+antec);

			}
			

			

			
			/*
			double antecsominv=getSommeInvAntecedants(k);
			if(antecsominv*k>4)
			System.out.println("k="+k+" antecsominv="+antecsominv*k+" log(k)="+Math.log(k));
		
			
			
			ArrayList<Long> antec=getAntecedants(k);
			System.out.println("k="+k+"  antec.size="+antec.size()+" antec="+antec);
			*/
		}
		
		
		
	}
	
	public static void main2205(String[] args) {
		int size=100000;
		double sommefatale=0;
		double sommefataleLog=0;
		ArrayList<Long> setrandom=rangeRandom(size, N_MAX*N_MAX);
		Iterator<Long> e=setrandom.iterator();
		int compteur=0;
		int compteurval=0;
		Map<Long, Long> countMap=new HashMap<>();

		while(e.hasNext())
		{
			long k=e.next();
			
			compteur++;

			long scurr=sigmaprime(k);
			if(scurr<N_MAX)
			{
				sommefatale=sommefatale+((1.0*scurr)/k);
				sommefataleLog=sommefataleLog+Math.log(((1.0*scurr)/k));
				Long seq=countMap.get(scurr);
                if(seq==null)
                {
                	seq=(long)0;
                }
                countMap.put(scurr, seq+1);

			}
			
			if(compteur%1000==0)
			{
			System.out.println("k="+k+"  rapport="+sommefatale/N_MAX+"  rapportlog="+sommefataleLog/N_MAX+"  compteur="+compteur+"  countMap="+countMap.size()/(1.0*N_MAX));

			}
			
		

		}

	}
	
	
	public static void main04062017(String[] args) {
		double sommefatale=0;
		double sommefataleLog=0;

		double rapport=0;
		
		for(long k=3;k<N_MAX*N_MAX;k++)
		{
			//if(k%2==0)
			{
				long scurr=sigmaprime(k);
				long scurr2=sigmaprime(scurr);

				if(scurr2<N_MAX && sigmapvalues.get(scurr)==null)
				{
					sommefatale=sommefatale+((1.0*scurr2)/scurr);
					//sommefataleLog=sommefataleLog+((1.0*scurr)/k)+1;
					sommefataleLog=sommefataleLog+1;
					sigmapvalues.put(scurr, k);

				}
				
				
			}
			if(k%10000==0)
			{
				System.out.println("k="+k+"  rapport="+sommefatale/N_MAX+"  rapportlog="+sommefataleLog/N_MAX);
			}
		}
		
		
		
	}
	


	

	public static void maincacluclsigmaprimecache(String[] args) {

		long compteur=0;
		double somme=0;
		
	 Map<Long, Long> someintestingvalues=new HashMap<>();

		for(long k=2;k<N_MAX;k++)
		{
		    //if(k%2==0)
		    {
				long sigmaprime1=sigmaprime(k);
				if(sigmaprime1<Math.sqrt(N_MAX)&& sigmaprime1>1)
				{
					Map<Long,Long> primeFactorization = primeFactorsMap(k);
					long smallfactor = N_MAX;
					for (long p:primeFactorization.keySet()) {
						somme=somme+(1.0/p);
					    if(p<smallfactor)
					    {
					    	smallfactor=p;
					    }
					}
					long smallfactorpower=(long) Math.pow(smallfactor, primeFactorization.get(smallfactor));
					
					Long smallrap=someintestingvalues.get(smallfactorpower);
					if(smallrap==null)
					{
						smallrap=(long) 0;
						someintestingvalues.put(smallfactorpower, smallrap);
					}
					someintestingvalues.put(smallfactorpower,smallrap+1);
					//somme=somme+(((1.0-(1.0/smallfactorpower))/(1.0-(1.0/smallfactor)))/smallfactor);
					
					//System.out.println("compteur="+(compteur++)+" k="+k+"  somme="+somme+"  rapport="+somme/Math.sqrt(N_MAX));
					System.out.println("n="+k+"  somme="+somme+"  rapport="+somme/Math.sqrt(N_MAX));

					//System.out.println("compteur="+(compteur++)+" k="+k+" kmod="+(1.0*k/primeref)+" mapk="+primeFactorsMap(k)+" sigmaprime1="+sigmaprime1+"  mapsigma="+primeFactorsMap(sigmaprime1));

				}
		    }


			//if(sigmaprime1>=N_MAX)
			/*
			long sigmaprime2=sigmaprime(sigmaprime1);
			if(sigmaprime2!=0 & sigmaprime2%primeref==0 && k%primeref!=0&& sigmaprime1%primeref!=0)
			{
				System.out.println("compteur="+(compteur++)+" k="+k+" kmod="+(1.0*k/primeref)+" mapk="+primeFactorsMap(k)+" sigmaprime2="+sigmaprime2+"  mapsigma="+primeFactorsMap(sigmaprime2));

			}
			*/

		}
		
		System.out.println(someintestingvalues);
		
		
	}

	public static void mainsacsa(String[] args) {
		double moyenne1=0;
		double moyenne2=0;
		double moyenne3=0;
		
		Map<Long, Long> countMap=new HashMap<>();
		
		long indicateur=2;
        long rest=0;
		double rapport=1.0/(indicateur*indicateur);
		double rapport2=1.0;

		for(long k=1;k<N_MAX;k++)
		{
			long i=(indicateur*k)+rest;
				long sigmaprime1=sigmaprime(i);
                Long seq=countMap.get(sigmaprime1);
                if(seq==null)
                {
                	seq=(long)0;
                }
                countMap.put(sigmaprime1, seq+1);
			
			
			if(k%10000==0)
            {
				int countmapsize=countMap.size();
    			rapport2=((double)countmapsize/k*Math.log(Math.log(k)));

    			System.out.println("k="+k+"  countMap="+countmapsize+" rapport="+rapport+"  rapport2="+rapport2);
            }
			

			//System.out.println("i="+i+"  s(i)="+sigmaprime1+"  sigma(s(i))="+sigmasigmaprime1);
		}
		
		System.out.println("countMap="+countMap.size());

	}
	
	public static void mainmoye(String[] args) {
		double moyenne1=0;
		double moyenne2=0;
		double moyenne3=0;

		
		for(long k=1;k<N_MAX;k++)
		{
			long i=2*k;
			long sigmaprime1=sigmaprime(i);
			moyenne1=moyenne1+(sigmaprime1/(double)i);
			moyenne3=moyenne3+Math.pow((sigmaprime1/(double)i),2);

			long sigmaprime2=sigmaprime(sigmaprime1);
			moyenne2=moyenne2+(sigmaprime2/(double)i);
            if(k%1000==0)
            {
    			System.out.println("i="+i+"  s(i)="+sigmaprime1+"  moyenne1="+(moyenne1/k)+"  moyenne2="+(moyenne2/k)+"  moyenne3="+(moyenne3/k));
            }

			//System.out.println("i="+i+"  s(i)="+sigmaprime1+"  sigma(s(i))="+sigmasigmaprime1);
		}

	}
	
	
	
	public static long sigma(long N)
	{
		Map<Long,Long> primeFactorization = primeFactorsMap(N);
		long total = 1;
		for (long p:primeFactorization.keySet()) {
		    long factor = 1;
		    for (long i=0;i<primeFactorization.get(p);i++) {
		        factor*=p;
		        factor+=1;
		    }
		    total*=factor;
		}
		return total;
		
	}
	
	public static long sigmaprime(long N)
	{
		Map<Long,Long> primeFactorization = primeFactorsMap(N);
		long total = 1;
		for (long p:primeFactorization.keySet()) {
		    long factor = 1;
		    for (long i=0;i<primeFactorization.get(p);i++) {
		        factor*=p;
		        factor+=1;
		    }
		    total*=factor;
		}
		return total-N;
		
	}
	
	public static AliquotNumber sigmaprime2(long N)
	{
		AliquotNumber an=new AliquotNumber();
		Map<Long,Long> primeFactorization = primeFactorsMap(N);
		an.setPrimeFactors(primeFactorization);
		long total = 1;
		for (long p:primeFactorization.keySet()) {
		    long factor = 1;
		    for (long i=0;i<primeFactorization.get(p);i++) {
		        factor*=p;
		        factor+=1;
		    }
		    total*=factor;
		}
		an.setSigma(total-N);
		return an;
		
	}
	public static AliquotNumber sigmaprime3(long N)
	{
		AliquotNumber an=new AliquotNumber();
		Map<Long,Long> mapfactors = primeFactorsMap(N);
		an.setPrimeFactors(mapfactors);
        long sommeDiviseur=1;
		Iterator<Long> ite=mapfactors.keySet().iterator();
		
		
		while(ite.hasNext())
		{
			long prime=ite.next();
			long power=mapfactors.get(prime);
			
			sommeDiviseur=sommeDiviseur*getSommeDiviserurPrimeProper(prime,power);
			
		}
		
		sommeDiviseur=sommeDiviseur-N;

		an.setSigma(sommeDiviseur);
		return an;
		
	}
	
	public static long sigmaprimecache(long N)
	{
		Long value=sigmapvalues.get(N);
		if(value==null)
		{
			value=sigmaprime(N);
			sigmapvalues.put(N, value);
		}
		return value;
		
	}
	public static Map<Long, ArrayList<Long>> getAntecedantsAll(long N)
	{
		long seuil=N*N;
		Map<Long, ArrayList<Long>> antecedantsvalues=new HashMap<>();
		
		
		for(long i=2;i<=seuil;i++)
		{
			long scur=sigmaprime(i);
			if(scur<N)
			{
				ArrayList<Long> antecedants =antecedantsvalues.get(scur);
				if(antecedants==null)
				{
					antecedants=new ArrayList<>();
				}
				antecedants.add(i);
				antecedantsvalues.put(scur, antecedants);
			}
			if(i%10000==0)
			{
				System.out.println("i="+i+"  antecedantsvalues="+antecedantsvalues.size());
			}
			
		}
		return antecedantsvalues;
		
	}
	
	public static ArrayList<Long> getAntecedants(long N)
	{
		long seuil=N*N;
		
		ArrayList<Long> antecedants = new ArrayList<>();
		
		for(long i=2;i<=seuil;i++)
		{
			long scur=sigmaprimecache(i);
			if(scur==N)
			{
				antecedants.add(i);
			}
			
			
		}
		return antecedants;
		
	}
	
	public static double getSommeInvlist(ArrayList<Long> liste)
	{
		double result=0;
		
		for(int i=0;i<liste.size();i++)
		{
				result=result+(1.0/liste.get(i));			
			
		}
		return result;
		
	}
	
	public static double getSommeInvAntecedants(long N)
	{
		double result=0;
		long seuil=N*N;
		
		for(long i=2;i<seuil;i++)
		{
			long scur=sigmaprimecache(i);
			if(scur==N)
			{
				result=result+(1.0/i);
			}
			
			
		}
		return result;
		
	}

	  public static Map<Long,Long> primeFactorsMapOld(long number) {
		    long n = number;
		    HashMap<Long, Long> mapfactors=new HashMap<>();
		    List<Long> factors = new ArrayList<Long>();
		    for (long i = 2; i <= n; i++) {
		      while (n % i == 0) {
		        factors.add(i);
		        if(mapfactors.get(i)==null)
		        {
		        	mapfactors.put(i, (long)0);
		        }
		        mapfactors.put(i,mapfactors.get(i)+1);
		        
		        n /= i;
		      }
		    }
		    return mapfactors;
		  }
	  
	  
	  public static Map<Long,Long> primeFactorsMap(long numbers) {
		    long n = numbers;
		    List<Long> factors = new ArrayList<Long>();
		    
		    Map<Long,Long> factorsMap = new HashMap<Long, Long>();

		    for (long i = 2; i <= n / i; i++) {
		      while (n % i == 0) {
		        factors.add(i);
		        
		        long compteur=0;
			      if(factorsMap.get(i)!=null)
			      {
			    	  compteur=factorsMap.get(i);
			      }
			      factorsMap.put(i, compteur+1);
		        
		        n /= i;
		      }
		    }
		    if (n > 1) {
		      factors.add(n);
		      long compteur=0;
		      if(factorsMap.get(n)!=null)
		      {
		    	  compteur=factorsMap.get(n);
		      }
		      factorsMap.put(n, compteur+1);
		      
		      
		    }
		    return factorsMap;
		  }
	  
	  
	  public  static ArrayList<Long> rangeRandom(long size,long max)
	  {
		  	Random r=new Random();
		  	ArrayList<Long> result=new ArrayList<>();
		  	for(int i=0;i<size;i++)
		  	{
		  		long cur=(long)r.nextInt((int)max);
		  		while(cur==0)
		  		{
			  	 cur=(long)r.nextInt((int)max);
		  		}
		  		result.add(cur);
		  	}
		  	
		  	
			return result;
		  
	  }
	  
		public static long getSommeDiviserurProper(long entier)
		{
			
			long sommeDiviseur=1;
			
			Map<Long,Long> mapfactors=primeFactorsMap(entier);
			

			Iterator<Long> ite=mapfactors.keySet().iterator();
			
			
			while(ite.hasNext())
			{
				long prime=ite.next();
				long power=mapfactors.get(prime);
				
				sommeDiviseur=sommeDiviseur*getSommeDiviserurPrimeProper(prime,power);
				
			}
			
			sommeDiviseur=sommeDiviseur-entier;
			
			
			
			return sommeDiviseur;
		}
		
		
		public static long getSommeDiviserurPrimeProper(long prime,long power)
		{
			long resultat=0;
			for(int i=0;i<power+1;i++)
			{
				resultat=resultat+(int) Math.pow(prime, i);
			}
			
			
			return resultat;
		}

}
