package toolsych;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Performanceur4 {

	public static long X_MAX=new Long(100000000);
	
	public static long periode=new Long(10000000);
	
	public static double[] tabvalueFn=new double[(int)(2*X_MAX)];
	
	
	
public static void main(String[] args) {

		
		Map<Long,List<Long>> mapValuesprime=new HashMap<>();
		Map<Long,List<Long>> mapValuenumb=new HashMap<>();

		long timeDebut=System.currentTimeMillis();
		double gamma=0.577215664901532860;
		double expGamma=Math.exp(gamma);
		double produitFn=1.0;
		long maxDifFnLoggamma=0;
		double rapportMax=0;
		for(long k=2;k<X_MAX;k++)
		{
			long i=k;
			long sommeDiviseur=1;
			
			long radical=1;
			 Map<Long,Long> mapfactors=primeFactorsMap(i);
			 Iterator<Long> e=mapfactors.keySet().iterator();
			 while(e.hasNext())
			 {
				 long curprime=e.next();
				 long power=mapfactors.get(curprime);
					
				sommeDiviseur=sommeDiviseur*getSommeDiviserurPrimeProper(curprime,power);
				 radical=radical*curprime;
			 }
			long sprime=sommeDiviseur-i;
			double expgammalogx=expGamma*Math.log(i);
			if(sprime==1)
			{
				produitFn=(produitFn*i)/(i-1);
			}
			
			double rapport=expgammalogx/produitFn;
			if(rapport>rapportMax)
			{
				rapportMax=rapport;
				maxDifFnLoggamma=i;
			}

			if(i%100000==0)
				System.out.println("i="+i+" produitFn="+produitFn+"   expgammalogx="+expgammalogx+"   rapportMax="+rapportMax+"   maxDifFnLoggamma="+maxDifFnLoggamma);
		}
		
		
		long timeFin=System.currentTimeMillis();
		long duree=timeFin-timeDebut;
		
		System.out.println(" mapValuesprime="+mapValuesprime.size());

		
		System.out.println("timing="+duree);
		
	}
	
	
	

public static void main2(String[] args) {

		
		Map<Long,Long> mapValuesprime=new HashMap<>();
		long timeDebut=System.currentTimeMillis();
		
		double somme=0.0;
		for(long k=2;k<X_MAX;k++)
		{
			long i=2*k;
			long sprime=getSommeDiviserurProper(i);
			if(sprime<X_MAX && sprime%2==0)
			{
				double rapport=(1.0*sprime)/i;

				tabvalueFn[(int)sprime]=tabvalueFn[(int)sprime]+rapport;

				somme=somme+rapport;
				
			}
			

			if(i%1000==0)
				System.out.println("i="+i+" somme="+somme+"  rapport="+somme/(1.0*X_MAX));
		}
		
		
		long timeFin=System.currentTimeMillis();
		long duree=timeFin-timeDebut;
		
		System.out.println(" mapValuesprime="+mapValuesprime.size()/(1.0*X_MAX));

		
		System.out.println("timing="+duree);
		
	}

	
	public static void mainOLD(String[] args) {

		
		Map<Long,List<Long>> mapValuesprime=new HashMap<>();
		Map<Long,List<Long>> mapValuenumb=new HashMap<>();

		long timeDebut=System.currentTimeMillis();
		
		
		for(long k=2;k<X_MAX;k++)
		{
			long i=2*k;
			long sommeDiviseur=1;
			long radical=1;
			 Map<Long,Long> mapfactors=primeFactorsMap(i);
			 Iterator<Long> e=mapfactors.keySet().iterator();
			 while(e.hasNext())
			 {
				 long curprime=e.next();
				 long power=mapfactors.get(curprime);
					
				sommeDiviseur=sommeDiviseur*getSommeDiviserurPrimeProper(curprime,power);
				 radical=radical*curprime;
			 }
			long sprime=sommeDiviseur-i;
			if(mapValuesprime.get(sprime)==null)
			{
				mapValuesprime.put(sprime, new ArrayList<Long>());
				mapValuenumb.put(sprime, new ArrayList<Long>());
			}
			if(mapValuesprime.get(sprime).contains(radical)) {
				System.out.println("i="+i+" mapValuesprime.get(sprime)="+mapValuesprime.get(sprime)+" mapValuenumb.get(sprime)="+mapValuenumb.get(sprime)+" radical en double="+radical);
			}
			mapValuesprime.get(sprime).add(radical);
			mapValuenumb.get(sprime).add(i);


			if(i%100000==0)
				System.out.println("i="+i+" sprime="+sprime);
		}
		
		
		long timeFin=System.currentTimeMillis();
		long duree=timeFin-timeDebut;
		
		System.out.println(" mapValuesprime="+mapValuesprime.size());

		
		System.out.println("timing="+duree);
		
	}
	
	

	
	public static Map<Long,Long> primeFactorsMap(long numbers) {
	    long n = numbers;
	    List<Long> factors = new ArrayList<Long>();
	    
	    Map<Long,Long> factorsMap = new HashMap<Long, Long>();

	    for (long i = 2; i <= n / i; i++) {
	      while (n % i == 0) {
	        factors.add(i);
	        
	        long compteur=0;
		      if(factorsMap.get(i)!=null)
		      {
		    	  compteur=factorsMap.get(i);
		      }
		      factorsMap.put(i, compteur+1);
	        
	        n /= i;
	      }
	    }
	    if (n > 1) {
	      factors.add(n);
	      long compteur=0;
	      if(factorsMap.get(n)!=null)
	      {
	    	  compteur=factorsMap.get(n);
	      }
	      factorsMap.put(n, compteur+1);
	      
	      
	    }
	    return factorsMap;
	  }
	
	
	public static long sigmaprime(long N)
	{
		Map<Long,Long> primeFactorization = primeFactorsMap(N);
		long total = 1;
		for (long p:primeFactorization.keySet()) {
		    long factor = 1;
		    for (long i=0;i<primeFactorization.get(p);i++) {
		        factor*=p;
		        factor+=1;
		    }
		    total*=factor;
		}
		return total-N;
		
	}
	
	public static long getSommeDiviserurProper(long entier)
	{
		
		long sommeDiviseur=1;
		
		Map<Long,Long> mapfactors=primeFactorsMap(entier);
		

		Iterator<Long> ite=mapfactors.keySet().iterator();
		
		
		while(ite.hasNext())
		{
			long prime=ite.next();
			long power=mapfactors.get(prime);
			
			sommeDiviseur=sommeDiviseur*getSommeDiviserurPrimeProper(prime,power);
			
		}
		
		sommeDiviseur=sommeDiviseur-entier;
		
		
		
		return sommeDiviseur;
	}
	
	
	public static long getSommeDiviserurPrimeProper(long prime,long power)
	{
		long resultat=0;
		for(int i=0;i<power+1;i++)
		{
			resultat=resultat+(long) Math.pow(prime, i);
		}
		
		
		return resultat;
	}

  

}
