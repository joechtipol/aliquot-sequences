package toolsych;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AliquotSystem {
	
	
	
	
	public static final long N_MAX=10000;
	public static final long primeref=37;

	

	public static void main(String[] args) {

		long compteur=0;
		
		for(long k=2;k<N_MAX;k++)
		{
			long sigmaprime1=sigmaprime(k);
			if(k%primeref!=0&& sigmaprime1%primeref==0)
			{
				System.out.println("compteur="+(compteur++)+" k="+k+" kmod="+k%primeref+" rapport="+(1.0*k/primeref)+" mapk="+primeFactorsMap(k)+" sigmaprime1="+sigmaprime1+"  mapsigma="+primeFactorsMap(sigmaprime1));

			}

			//if(sigmaprime1>=N_MAX)
			
			long sigmaprime2=sigmaprime(sigmaprime1);
			if(sigmaprime2!=0 & sigmaprime2%primeref==0 && k%primeref!=0&& sigmaprime1%primeref!=0)
			{
				//System.out.println("compteur="+(compteur++)+" k="+k+" kmod="+k%primeref+" rapport="+(1.0*k/primeref)+" mapk="+primeFactorsMap(k)+" sigmaprime2="+sigmaprime2+"  mapsigma="+primeFactorsMap(sigmaprime2));

			}
			

		}
	}

	public static void main222(String[] args) {
		double moyenne1=0;
		double moyenne2=0;
		double moyenne3=0;
		
		Map<Long, Long> countMap=new HashMap<>();
		
		long indicateur=2;
        long rest=1;
		double rapport=1.0/(indicateur*indicateur);
		double rapport2=1.0;

		for(long k=1;k<N_MAX;k++)
		{
			long i=(indicateur*k)+rest;
				long sigmaprime1=sigmaprime(i);
                Long seq=countMap.get(sigmaprime1);
                if(seq==null)
                {
                	seq=(long)0;
                }
                countMap.put(sigmaprime1, seq+1);
			
			
			if(k%10000==0)
            {
				int countmapsize=countMap.size();
    			rapport2=((double)countmapsize/k);

    			System.out.println("k="+k+"  countMap="+countmapsize+" rapport="+rapport+"  rapport2="+rapport2);
            }
			

			//System.out.println("i="+i+"  s(i)="+sigmaprime1+"  sigma(s(i))="+sigmasigmaprime1);
		}
		
		System.out.println("countMap="+countMap.size());

	}
	
	public static void mainmoye(String[] args) {
		double moyenne1=0;
		double moyenne2=0;
		double moyenne3=0;

		
		for(long k=1;k<N_MAX;k++)
		{
			long i=2*k;
			long sigmaprime1=sigmaprime(i);
			moyenne1=moyenne1+(sigmaprime1/(double)i);
			moyenne3=moyenne3+Math.pow((sigmaprime1/(double)i),2);

			long sigmaprime2=sigmaprime(sigmaprime1);
			moyenne2=moyenne2+(sigmaprime2/(double)i);
            if(k%1000==0)
            {
    			System.out.println("i="+i+"  s(i)="+sigmaprime1+"  moyenne1="+(moyenne1/k)+"  moyenne2="+(moyenne2/k)+"  moyenne3="+(moyenne3/k));
            }

			//System.out.println("i="+i+"  s(i)="+sigmaprime1+"  sigma(s(i))="+sigmasigmaprime1);
		}

	}
	
	
	
	public static long sigma(long N)
	{
		Map<Long,Long> primeFactorization = primeFactorsMap(N);
		long total = 1;
		for (long p:primeFactorization.keySet()) {
		    long factor = 1;
		    for (long i=0;i<primeFactorization.get(p);i++) {
		        factor*=p;
		        factor+=1;
		    }
		    total*=factor;
		}
		return total;
		
	}
	
	public static long sigmaprime(long N)
	{
		Map<Long,Long> primeFactorization = primeFactorsMap(N);
		long total = 1;
		for (long p:primeFactorization.keySet()) {
		    long factor = 1;
		    for (long i=0;i<primeFactorization.get(p);i++) {
		        factor*=p;
		        factor+=1;
		    }
		    total*=factor;
		}
		return total-N;
		
	}
	

	  public static Map<Long,Long> primeFactorsMap(long number) {
		    long n = number;
		    HashMap<Long, Long> mapfactors=new HashMap<>();
		    List<Long> factors = new ArrayList<Long>();
		    for (long i = 2; i <= n; i++) {
		      while (n % i == 0) {
		        factors.add(i);
		        if(mapfactors.get(i)==null)
		        {
		        	mapfactors.put(i, (long)0);
		        }
		        mapfactors.put(i,mapfactors.get(i)+1);
		        
		        n /= i;
		      }
		    }
		    return mapfactors;
		  }

}
