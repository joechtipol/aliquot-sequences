package toolsych;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Performanceur1 {

	public static long X_MAX = new Long(10000000);

	public static long periode = new Long(10000000);

	public static void main(String[] args) {

		Map<Long, Long> mapValuesprime = new HashMap<>();
		long timeDebut = System.currentTimeMillis();
        long compteur=0;
		double somme = 0.0;

		for (long i = 2; i < X_MAX; i++) {
			//if(i%2!=0)
			{
				long k=(2*i);
				//long spimeI=sigmaprime(i);
				long sprime = sigmaprime(k);
				//if (sprime < X_MAX && sprime%2==0) 
				//if (spimeI < X_MAX/3) 
				if (sprime < X_MAX) 
				{
					compteur++;
					//long spimeI=sigmaprime(i);

					//somme = somme + (1.0 * spimeI / (i));
					somme = somme + (1.0 * sprime / (k));

					/*
					if (mapValuesprime.get(sprime) == null) {
						mapValuesprime.put(sprime, i);
					}
                    */
				}
				
			}
			

			if (i % 1000 == 0)
				System.out.println("i=" + i + " compteur=" + compteur + "  rapport="
						+ mapValuesprime.size() / (1.0 * X_MAX) + "  somme=" + somme);
		}

		long timeFin = System.currentTimeMillis();
		long duree = timeFin - timeDebut;

		System.out.println(" mapValuesprime=" + mapValuesprime.size() / (1.0 * X_MAX));

		System.out.println("timing=" + duree);

	}

	public static Map<Long, Long> primeFactorsMap(long numbers) {
		long n = numbers;
		List<Long> factors = new ArrayList<Long>();

		Map<Long, Long> factorsMap = new HashMap<Long, Long>();

		for (long i = 2; i <= n / i; i++) {
			while (n % i == 0) {
				factors.add(i);

				long compteur = 0;
				if (factorsMap.get(i) != null) {
					compteur = factorsMap.get(i);
				}
				factorsMap.put(i, compteur + 1);

				n /= i;
			}
		}
		if (n > 1) {
			factors.add(n);
			long compteur = 0;
			if (factorsMap.get(n) != null) {
				compteur = factorsMap.get(n);
			}
			factorsMap.put(n, compteur + 1);

		}
		return factorsMap;
	}

	public static long sigmaprime(long N) {
		Map<Long, Long> primeFactorization = primeFactorsMap(N);
		long total = 1;
		for (long p : primeFactorization.keySet()) {
			long factor = 1;
			for (long i = 0; i < primeFactorization.get(p); i++) {
				factor *= p;
				factor += 1;
			}
			total *= factor;
		}
		return total - N;

	}
	
	
	public static long divisorSum(long n) 
    { 
        long sum = 0; 
  
        long i=n;  
            // Find all divisors of i 
            // and add them 
            for (int j = 1; j * j <= i; ++j) { 
                if (i % j == 0) { 
                    if (i / j == j) 
                        sum += j; 
                    else
                        sum += j + i / j; 
                } 
            } 
       
        return sum-n; 
    } 
	
	
	public static long sum_of_divisors(long number)
	{
		long sum = 1;
		long n=number;

		for(long k = 2; k * k <= n; ++k)
		{
			long p = 1;
			while(n % k == 0)
			{
				p = p * k + 1;
				n /= k;	
			}

			sum *= p;
		}

		if(n > 1)
			sum *= 1 + n;

		return sum-number;
	}

}
