package toolsych.db.cassandra;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

import toolsych.AliquotSystemConj;
import toolsych.structure.AliquotNumber;

public class CassandraFactorsRunnable implements Runnable {

	protected long minValue;

	protected long maxValue;

	protected String codeHashRunnable;

	static String[] CONTACT_POINTS = {"127.0.0.1"};
    static int PORT = 9042;
	
    
    private Cluster cluster;

    private Session session;

    /**
     * Initiates a connection to the cluster
     * specified by the given contact point.
     *
     * @param contactPoints the contact points to use.
     * @param port          the port to use.
     */
    public void connect(String[] contactPoints, int port) {

        cluster = Cluster.builder()
                .addContactPoints(contactPoints).withPort(port)
                .build();

        System.out.printf("Connected to cluster: %s%n", cluster.getMetadata().getClusterName());

        session = cluster.connect();
    }
    
	
	public CassandraFactorsRunnable(long minValue, long maxValue) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		codeHashRunnable = minValue + "-to-" + maxValue;

	}
	
	public  void testUseMariaDbInsertFactors()
	{

		try {
		    
			
			connect(CONTACT_POINTS, PORT);
			for(long i=minValue;i<maxValue;i++)
			{
				AliquotNumber an=AliquotSystemConj.sigmaprime3(i);
				String factors=an.getPrimeFactors().toString();
				long sprime=an.getSigma();
				
				
				String insertsql="INSERT INTO aliquot_cas.aliquot_seq_factors (number, factors,sprime,sprime2) VALUES ("+i+",'"+factors+"',"+sprime+",0)";
				

		        session.execute(insertsql);
		        
		        
               if(i%1000==0)
				System.out.println("code thread="+codeHashRunnable+" i="+i);				
			}
			
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			try {
				close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
	}
	

	@Override
	public void run() {
		long timeDebut=System.currentTimeMillis();

		testUseMariaDbInsertFactors();
		/*
		try {
			testUseMariaDbInsertCarreSprime();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		long timeFin=System.currentTimeMillis();
		long duree=timeFin-timeDebut;
		

		
		System.out.println("code thread="+codeHashRunnable+"  timing="+duree);
	}
	
	
	public  void testUseMariaDbInsertCarreSprime() throws SQLException {
		Connection connection = DriverManager
				.getConnection("jdbc:mariadb://localhost:3306/aliquot_1?user=root&password=root");
		try {
//58094
			for (long K = minValue; K < maxValue; K++) {
				long j = 2 * K + 1;
				double sommeDiv = 1.0;
				BigDecimal sommeDivCarre = new BigDecimal(1.0);
				Map<Long, Long> factorsMap = AliquotSystemConj.primeFactorsMap(j);
				Map<Long, Long> factorsMapCarre = new HashMap<>();

				Iterator<Long> e = factorsMap.keySet().iterator();

				while (e.hasNext()) {
					long currentP = e.next();
					long power = factorsMap.get(currentP);
					factorsMapCarre.put(currentP, 2 * power);
					sommeDiv = sommeDiv * AliquotSystemConj.getSommeDiviserurPrimeProper(currentP, power);

					sommeDivCarre = sommeDivCarre.multiply(getSommeDiviserurPrimeProper(currentP, 2 * power));

				}
				sommeDiv = sommeDiv - j;

				BigDecimal bd = new BigDecimal(j).pow(2);
				sommeDivCarre = sommeDivCarre.subtract(bd);

				String insertsql = "";
				try {

					Statement stmt = connection.createStatement();
                    if(sommeDivCarre.longValueExact()>50000)
                    {
					String factorsStr = factorsMapCarre.toString();
					insertsql = "INSERT INTO aliquot_seq_factors (number, factors,sprime,sprime2) VALUES ("
							+ bd.toString() + ",'" + factorsStr + "'," + sommeDivCarre.toString() + ",0)";
					//System.out.println(insertsql);
					stmt.executeQuery(insertsql);
                    }

				} catch (Exception f) {
					f.printStackTrace();
				}

				if (K % 1000 == 0)
					System.out.println("code thread="+codeHashRunnable+" i="+K);

					//System.out.println("j=" + j + "  sommeDiv=" + sommeDiv + "  sommeDivCarre=" + sommeDivCarre);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static BigDecimal getSommeDiviserurPrimeProper(long prime, long power) {
		BigDecimal resultat = BigDecimal.ZERO;
		for (int i = 0; i < power + 1; i++) {
			resultat = resultat.add((new BigDecimal(prime)).pow(i));
		}

		return resultat;
	}


	public long getMinValue() {
		return minValue;
	}

	public void setMinValue(long minValue) {
		this.minValue = minValue;
	}

	public long getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(long maxValue) {
		this.maxValue = maxValue;
	}

	public String getCodeHashRunnable() {
		return codeHashRunnable;
	}

	public void setCodeHashRunnable(String codeHashRunnable) {
		this.codeHashRunnable = codeHashRunnable;
	}

	
	/**
     * Closes the session and the cluster.
     */
    public void close() {
        session.close();
        cluster.close();
    }
}
