package toolsych;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ColossalyCalcul1 {
	
	
	public static List<Long> myPrimes=new ArrayList<>();
	public static List<Double> myTetaPrimes=new ArrayList<>();

	
	
	public static final long XMAX=10000000;
	
	public static final long XPMAX=50;
	
	public static final long DIVISOR=50;

	
	public static String  MODE_OS="WIN";

	//public static Map<BigDecimal,String> bigMap=new HashMap<>();
	public static Map<String, BigDecimal> bigMap=new HashMap<>();


	public static void main(String[] args) throws IOException {
		
		//Map<String, BigDecimal> bigMap=calculBigMap(XMAX);
		bigMap=calculBigMap(XMAX);
		if(args.length>0)
		{
			MODE_OS=args[0];
			
			System.out.println("NMAW recharged="+MODE_OS);
		}
		
		String path="c:/data/epsilons_ca_list.txt";
		if(!MODE_OS.equals("WIN"))
		{
			path="/home/ec2-user/aliquot-data/epsilons_ca_list.txt";
		}
		
		
		FileOutputStream fos=new FileOutputStream(path);
		
		List<BigDecimal> initPrimes=new ArrayList<>();

		
		
		
		Iterator<String> e=bigMap.keySet().iterator();
		while(e.hasNext())
		{
			String key=e.next();
			BigDecimal value=bigMap.get(key);
			if(value.doubleValue()>0)
			initPrimes.add(value);
		}
		System.out.println(initPrimes.size()+"  kkkkkkkk");
		
		Collections.sort(initPrimes);
		double sommepsilon=0;
		
		Map<Double, Long> maplogs=new HashMap<>();
		
		
		
		
		BigDecimal epsilonMax=new BigDecimal("1.857851346175930761114704260986515E-8");
		
		
		List<List<BigDecimal>>  ttlistes=diviserlst(initPrimes);
		List<ColossalyCalculRunnable>  ccrs=new ArrayList<>();

		
		for(int i=0;i<ttlistes.size();i++)
		{
			ColossalyCalculRunnable ccr=new ColossalyCalculRunnable(ttlistes.get(i));
			ccrs.add(ccr);
			new Thread(ccr).start();
		}
		
		
		//System.out.println(ttlistes);
		
		/*
		for(int i=0;i<initPrimes.size();i++)
		{
			BigDecimal currentEpsilo=initPrimes.get(initPrimes.size()-i-1);
			//if(currentEpsilo.compareTo(epsilonMax)<0)
			{
				sommepsilon=sommepsilon+currentEpsilo.doubleValue();
				List<Long> factorsCA=calculerN0List(currentEpsilo);
				double logN=calculLogN(factorsCA);
				BigDecimal abundance=new BigDecimal(Math.exp(calculAbundance(factorsCA)));
				
				BigDecimal extremAbundance=abundance.divide(new BigDecimal(Math.log(logN)),MathContext.DECIMAL128);
				
				abundance=abundance.setScale(8, BigDecimal.ROUND_UP);
				
				
				

				if(maplogs.get(logN)!=null)
				{
					System.out.println("   Exception"+(i+1)+"  "+currentEpsilo+"  "+factorsCA+"  "+logN+"  "+sommepsilon);

				}
				else
				{
					maplogs.put(logN, (long) 1);
				}
				String strFile=""+(i+1)+";"+currentEpsilo+";"+factorsCA+";"+logN+";"+abundance+";"+extremAbundance+'\n';
				fos.write(strFile.getBytes());
				System.out.println(""+(i+1)+"  "+currentEpsilo+"  "+factorsCA+"  "+logN+"  "+abundance+";"+extremAbundance);
	
			}
			
		}*/

fos.close();
		System.out.println(bigMap.size());

	}

	
	
	
	
	public static Map<String,BigDecimal> calculBigMap(long x)
	{
		Map<String,BigDecimal> primes=new HashMap<>();
		double sommeLogTeta=0;
		for(long i=2;i<=x;i++)
		{
			long sprime=AliquotSystemConj.sigmaprime(i);
			if(sprime==1)
			{
				myPrimes.add(i);
				sommeLogTeta=sommeLogTeta+Math.log(i);
				myTetaPrimes.add(sommeLogTeta);
				BigDecimal sommeBas=BigDecimal.ZERO;
				BigDecimal currentEpsilon=BigDecimal.ZERO;
				long kmax=XPMAX;
				if(i>100000)
				{
					kmax=3;
				}
				
				for(int k=1;k<kmax;k++)
				{
					sommeBas=sommeBas.add(new BigDecimal(Math.pow(i, k)));
					
					currentEpsilon=(new BigDecimal(Math.log((1.0+(1.0/sommeBas.doubleValue()))))).divide(new BigDecimal(Math.log(i)),MathContext.DECIMAL128);
					primes.put(i+";"+k, currentEpsilon);
				}
				
			}
			
			
			if(i%10000==0)
			{
				System.out.println(i+"  "+primes.size());
			}

		}
		
		
		return primes;
		
	}
	
	
	public static List<Long> getPrimes(long x)
	{
		List<Long> primes=new ArrayList<>();
		
		for(long i=2;i<=x;i++)
		{
			long sprime=AliquotSystemConj.sigmaprime(i);
			if(sprime==1)
			{
				primes.add(i);
			}

		}
		
		
		return primes;
		
	}
	
	
	
	

	public static String calculerN0(BigDecimal epsilonzero)
	{
		
		String listFactors="{";
		int exp2=0;
		{
			long currentPrime=2;
			
			exp2= getExposantPrimeByEpsilon(epsilonzero, currentPrime);
		}
		
		
		if(exp2>0)
		{
			long [] tabPrimes=new long[exp2];
			
			for(int i=0;i<myPrimes.size();i++)
			{
				long currentPrime=myPrimes.get(i);
				//if(currentPrime<x)
				{
					int facteureps=getExposantPrimeByEpsilon(epsilonzero, currentPrime);
					if(facteureps>0)
					{
						//listFactors.add(currentPrime+":"+facteureps);
						tabPrimes[exp2-facteureps]=currentPrime;

					}
					else
					{
						break;
					}
				}
				
				/*else {
					break;
				}*/
				

			}	
			
			for(int i=0;i<tabPrimes.length;i++)
			{
				if(i==0)
				{
					listFactors=listFactors+tabPrimes[tabPrimes.length-i-1];
				}
				else
				{
					listFactors=listFactors+","+tabPrimes[tabPrimes.length-i-1];

				}
			}
		}
		
		
		

		
		listFactors=listFactors+"}";
		
		
		
      return listFactors;
	}
	
	
	
	public static List<Long> calculerN0List(BigDecimal epsilonzero)
	{
		
		List<Long> listFactors=new ArrayList<>();
		int exp2=0;
		{
			long currentPrime=2;
			
			exp2= getExposantPrimeByEpsilon(epsilonzero, currentPrime);
		}
		
		
		if(exp2>0)
		{
			long [] tabPrimes=new long[exp2];
			
			for(int i=0;i<myPrimes.size();i++)
			{
				long currentPrime=myPrimes.get(i);
				//if(currentPrime<x)
				{
					int facteureps=getExposantPrimeByEpsilon(epsilonzero, currentPrime);
					if(facteureps>0)
					{
						tabPrimes[exp2-facteureps]=currentPrime;

					}
					else
					{
						break;
					}
				}
				
				/*else {
					break;
				}*/
				

			}
			
			for(int i=0;i<tabPrimes.length;i++)
			{
		
					listFactors.add(tabPrimes[tabPrimes.length-i-1]);

			}
			
			
			
		}		
		
		
      return listFactors;
	}
	
	
	
	
	
	
	
	public static int getExposantPrimeByEpsilon(BigDecimal epsilonzero,Long onePrime)
	{
		
		BigDecimal denominateur=new BigDecimal(Math.pow(onePrime, epsilonzero.doubleValue()+1)-1);
		BigDecimal sousnominateur=new BigDecimal(Math.pow(onePrime, epsilonzero.doubleValue())-1);
		BigDecimal bd=denominateur.divide(sousnominateur,MathContext.DECIMAL128);

		BigDecimal epsilon=(new BigDecimal(Math.log(bd.doubleValue()))).divide(new BigDecimal(Math.log(onePrime)),MathContext.DECIMAL128);
		epsilon=epsilon.setScale(8, BigDecimal.ROUND_UP);

		int facteureps=(epsilon.intValue() -1);
		
		return facteureps;
		
		
		
	}
	
	public static double calculLogN(List<Long> factors)
	{
		double result=0;
		int intincr=1;
		for(int i=0;i<factors.size();i++)
		{
			long curFactor=factors.get(i);
			if(curFactor>0)
			{
				int indexprime=myPrimes.indexOf(curFactor);
				double tetaprime=myTetaPrimes.get(indexprime);
				result=result+(intincr*tetaprime);
				intincr=1;
			}
			else
			{
				intincr++;
			}
		}
		return result;
		
	}
	
	

	
	public static Map<BigDecimal,String> calculBigMapinv(long x)
	{
		Map<BigDecimal,String> primes=new HashMap<>();
		double sommeLogTeta=0;
		for(long i=2;i<=x;i++)
		{
			long sprime=AliquotSystemConj.sigmaprime(i);
			if(sprime==1)
			{
				myPrimes.add(i);
				sommeLogTeta=sommeLogTeta+Math.log(i);
				myTetaPrimes.add(sommeLogTeta);
				BigDecimal sommeBas=BigDecimal.ZERO;
				BigDecimal currentEpsilon=BigDecimal.ZERO;
				long kmax=XPMAX;
				if(i>100000)
				{
					kmax=10;
				}
				
				for(int k=1;k<kmax;k++)
				{
					sommeBas=sommeBas.add(new BigDecimal(Math.pow(i, k)));
					
					currentEpsilon=(new BigDecimal(Math.log((1.0+(1.0/sommeBas.doubleValue()))))).divide(new BigDecimal(Math.log(i)),MathContext.DECIMAL128);
					primes.put( currentEpsilon,i+";"+k);
				}
				
			}
			
			
			if(i%10000==0)
			{
				System.out.println(i+"  "+primes.size());
			}

		}
		
		
		return primes;
		
	}
	
	
	public static double calculAbundance(List<Long> factors)
	{
		double result=0;
		int intincr=1;
		int size=factors.size();
		long maxPrime=factors.get(0);
		
		for(int i=0;i<myPrimes.size();i++)
		{
			long currPrime=myPrimes.get(i);
			if(currPrime>maxPrime)
			{
				break;
			}
			else
			{
				for(int j=size-1;j>=0;j--)
				{
					long currentFactor=factors.get(j);
					if(currentFactor>0)
					{
						if(currPrime<=currentFactor)
						{
							result=result+getLogSigmapsurp(currPrime, j+1);
							j=-1;
						}
					}
					
				}
			}	
		}
		return result;
	}
	
	
	
	
	public static double getLogSigmapsurp(long anyPrime,long anyExponent)
	{
		double result=0;
		BigDecimal sommeInverses=BigDecimal.ONE;
		BigDecimal invAnyPrime=new BigDecimal(1.0/anyPrime);
		for(int i=1;i<anyExponent+1;i++)
		{
			sommeInverses=sommeInverses.add(invAnyPrime.pow(i));
		}
		
		result=Math.log(sommeInverses.doubleValue());
		return result;
	}
	
	
	public static List<List<BigDecimal>> diviserlst(List<BigDecimal> epsilons)
	{
		long nombre=(epsilons.size())/DIVISOR;
		
		List<List<BigDecimal>> result=new ArrayList<>();
		List<BigDecimal> resultint=new ArrayList<>();

		for(int i=0;i<epsilons.size();i++)
		{
			resultint.add(epsilons.get(i));
			if(i%nombre==0)
			{
			   result.add(resultint);
			   resultint=new ArrayList<>();
			   resultint.add(epsilons.get(i));

			}
		}
		
		return result;
	}
	
	
	
	
}
