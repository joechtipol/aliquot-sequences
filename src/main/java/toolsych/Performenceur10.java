package toolsych;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Performenceur10 {
	
	public static void main(String[] args) throws IOException {
		FileInputStream fis=new FileInputStream("c:/data/SAN_modified_HXA_1000000.txt");
		

		InputStreamReader isr=new InputStreamReader(fis);
		BufferedReader br=new BufferedReader(isr);
		String line=null;
		
		
		Map<Long, List<Double>> mapInfoXHA=new HashMap<>();
		Map<Long, String> mapInfoFactorsXHA=new HashMap<>();

		while((line=br.readLine())!=null)
		{
			String[] tab=line.trim().split("  ");
			
			double muPlusOne=Double.parseDouble(tab[0].trim());
			double hypeabundance=Double.parseDouble(tab[1].trim());
			long indexMax=Long.parseLong(tab[2].trim());
			String factors=tab[3].trim()+" "+hypeabundance;
			
			if(mapInfoXHA.get(indexMax)==null)
			{
				mapInfoXHA.put(indexMax, new ArrayList<Double>());
			}
			mapInfoXHA.get(indexMax).add(muPlusOne);

			mapInfoFactorsXHA.put(indexMax, factors);
			
			
			//System.out.println("muPlusOne="+muPlusOne+"  hypeabundance="+hypeabundance+"  indexMax="+indexMax);

			
			
		}
		
	
		
		Map<Long, List<Double>> mapInfoXHASorted=new HashMap<>();
		ArrayList<Long> indexsM=new ArrayList<>();
		
		Iterator<Long> e=mapInfoXHA.keySet().iterator();
		
		while(e.hasNext())
		{
			long key=e.next();
			List<Double> list=mapInfoXHA.get(key);
			Collections.sort(list);
			indexsM.add(key);
			//System.out.println("---index="+key+"  lesMus="+list);
		}
		
		Collections.sort(indexsM);
		
		e=indexsM.iterator();
		
		while(e.hasNext())
		{
			long key=e.next();
			List<Double> list=mapInfoXHA.get(key);
			System.out.println("---index="+key+" "+mapInfoFactorsXHA.get(key)+"  muMin="+list.get(0)+"  muMax="+list.get(list.size()-1));
		}
		
		

		
		
		
		//System.out.print(mapInfoXHA);
		
		
		
		
		br.close();
		
		
	}

	public static void mainwrite(String[] args) throws IOException {
		FileOutputStream fos=new FileOutputStream("c:/data/SAN_modified_HXA_1000000.txt");

		List<Long> initPrimes=getPrimes(1000000);

		List<Double> listMu=readMuVas();
		
		for(int i=0;i<listMu.size();i++)
		{
			double currentMu=listMu.get(i);
			if(currentMu<1.01)
			runSystemForMu(listMu.get(i),initPrimes,fos);
		}
		
		fos.close();

	}
	
	
	
	
	public static double logByFactors(Map<BigInteger, Long> mapfacteurs)
	{ 
		double log=0;
		Iterator<BigInteger> e=mapfacteurs.keySet().iterator();
		while(e.hasNext())
		{
			BigInteger key=e.next();
			long value=mapfacteurs.get(key);
			log=log+(value*(Math.log(key.longValue())));
		}
		
		
		
		return log;
	}
	
	public static double sigmaNsurNByFactors(Map<BigInteger, Long> mapfacteurs,int degree)
	{ 
		double log=1;
		Iterator<BigInteger> e=mapfacteurs.keySet().iterator();
		while(e.hasNext())
		{
			BigInteger key=e.next();
			long value=mapfacteurs.get(key);
			if(value>=2)
			{
				log=log*getSommeDiviserurPrimeProper(key.longValue(),value);		
			}
		}
		
		
		
		return log;
	}
	
	
	
	public static double getSommeDiviserurPrimeProper(long prime,long power)
	{
		double resultat=0;
		for(int i=0;i<power+1;i++)
		{
			resultat=resultat+Math.pow(prime, -i);
		}
		
		
		return resultat;
	}
	
	
	public static List<Long> getPrimes(long XMAX)
	{
		List<Long> primes=new ArrayList<>();
		
		for(long i=2;i<=XMAX;i++)
		{
			long sprime=Performanceur4.sigmaprime(i);
			if(sprime==1)
			{
				primes.add(i);
			}

		}
		
		
		return primes;
		
	}
	public static List<Double> getSommeMagicPrimes(List<Long> primes,long seuil)
	{
		List<Double> primesProduct=new ArrayList<>();
		double sommeInv=0.0;
		long oldPrime=0;
		for(long i=0;i<primes.size();i++)
		{
			long currentprime=primes.get((int)i);
			for(long j=oldPrime;j<currentprime;j++)
			{
				primesProduct.add(sommeInv);
			}
			sommeInv=sommeInv+((1.0)/(currentprime));
			oldPrime=currentprime;
		}
		
		for(long j=oldPrime;j<seuil;j++)
		{
			primesProduct.add(sommeInv);
		}
		
		
		return primesProduct;
		
	}
	
	
	
	public static List<Double> getProductMagicPrimes(List<Long> primes,long seuil)
	{
		List<Double> primesProduct=new ArrayList<>();
		double product=1.0;
		long oldPrime=0;
		for(long i=0;i<primes.size();i++)
		{
			long currentprime=primes.get((int)i);
			for(long j=oldPrime;j<currentprime;j++)
			{
				primesProduct.add(product);
			}
			product=product*((1.0*currentprime)/(currentprime-1));
			oldPrime=currentprime;
		}
		
		for(long j=oldPrime;j<seuil;j++)
		{
			primesProduct.add(product);
		}
		
		
		return primesProduct;
		
	}
	
	
	
	
	public static List<String> convertMapSorted(Map<BigInteger, Long> facteurs)
	{
		ArrayList<String> result=new ArrayList<>();
		ArrayList<BigInteger> listeFacteurs=new ArrayList<>();		

		Iterator<BigInteger> e=facteurs.keySet().iterator();
		while(e.hasNext())
		{
			BigInteger key=e.next();
			listeFacteurs.add(key);
			
		}
		
		Collections.sort(listeFacteurs);
		for(int i=0;i<listeFacteurs.size();i++)
		{
			String current=listeFacteurs.get(i)+":"+facteurs.get(listeFacteurs.get(i));
			result.add(current);
		}
		
		
		
		
		
		
		
		
		return result;
		
		
		
	}
	
	
	
	
	
	
	
	public static List<Double> readMuVas() throws IOException
	{		
		
		FileInputStream fis=new FileInputStream("c:/data/SAN_modified_mu_ca_1000000.txt");

		InputStreamReader isr=new InputStreamReader(fis);
		
		List<Double> myLst=new ArrayList<>();
		String line=null;
		BufferedReader br=new BufferedReader(isr);

		while((line=br.readLine())!=null)
		{
			String[] tab=line.trim().split(";");
			myLst.add(Double.parseDouble(tab[1]));
		}
		
		
		return myLst;
	}
	
	
	
	public static String calculerN0(double epsilonzero,double x,List<Long> primes)
	{
		
		String listFactors="{";
		int exp2=0;
		{
			long currentPrime=2;
			
				double denominateur=Math.pow(currentPrime, epsilonzero+1)-1;
				double sousnominateur=Math.pow(currentPrime, epsilonzero)-1;

				double epsilon=Math.log((denominateur/sousnominateur))/Math.log(currentPrime);
				
				exp2=((int) epsilon) -1;
		}
		
		
		if(exp2>0)
		{
			long [] tabPrimes=new long[exp2];
			
			for(int i=0;i<primes.size();i++)
			{
				long currentPrime=primes.get(i);
				if(currentPrime<x)
				{
					double denominateur=Math.pow(currentPrime, epsilonzero+1)-1;
					double sousnominateur=Math.pow(currentPrime, epsilonzero)-1;

					double epsilon=Math.log((denominateur/sousnominateur))/Math.log(currentPrime);
					
					int facteureps=((int) epsilon) -1;
					if(facteureps>0)
					{
						//listFactors.add(currentPrime+":"+facteureps);
						tabPrimes[exp2-facteureps]=currentPrime;

					}
				}
				
				else {
					break;
				}
				

			}	
			
			for(int i=0;i<tabPrimes.length;i++)
			{
				if(i==0)
				{
					listFactors=listFactors+tabPrimes[tabPrimes.length-i-1];
				}
				else
				{
					listFactors=listFactors+","+tabPrimes[tabPrimes.length-i-1];

				}
			}
		}
		
		
		

		
		listFactors=listFactors+"}";
		
		
		
      return listFactors;
	}
	
	
	public static List<Long> calculerN0List(double epsilonzero,double x,List<Long> primes)
	{
		
		ArrayList<Long> listFactors=new ArrayList<>();
		int exp2=0;
		{
			long currentPrime=2;
			
				double denominateur=Math.pow(currentPrime, epsilonzero+1)-1;
				double sousnominateur=Math.pow(currentPrime, epsilonzero)-1;

				double epsilon=Math.log((denominateur/sousnominateur))/Math.log(currentPrime);
				
				exp2=((int) epsilon) -1;
		}
		
		long [] tabPrimes=new long[exp2];
		
		for(int i=0;i<primes.size();i++)
		{
			long currentPrime=primes.get(i);
			if(currentPrime<x)
			{
				double denominateur=Math.pow(currentPrime, epsilonzero+1)-1;
				double sousnominateur=Math.pow(currentPrime, epsilonzero)-1;

				double epsilon=Math.log((denominateur/sousnominateur))/Math.log(currentPrime);
				
				int facteureps=((int) epsilon) -1;
				if(facteureps>0)
				{
					//listFactors.add(currentPrime+":"+facteureps);
					tabPrimes[exp2-facteureps]=currentPrime;

				}
			}
			
			else {
				break;
			}
			

		}
		
		
		for(int i=0;i<tabPrimes.length;i++)
		{
			listFactors.add(tabPrimes[tabPrimes.length-i-1]);
		}
		
		
		
		
      return listFactors;
	}
	
	
	
	
	
	
	
	
	public static void runSystemForMu(double mu,List<Long> initPrimes,FileOutputStream fos) throws IOException
	{

		FileInputStream fis=new FileInputStream("c:/data/SAN_1000000.txt");
		

		InputStreamReader isr=new InputStreamReader(fis);
		BufferedReader br=new BufferedReader(isr);
		double somme=0;
		long compteur=0;
		String line=null;
		double maxRm=Double.MAX_VALUE;
		double maxRmLogLog=0;

		long maxM=0;
		long compteurCA=0;
		long compteurXA=0;
		long compteurXCA=0;
		long compteurXSA=0;


		long compteurepsilonInfLog=0;
		long compteurepsilonSupLog=0;

		double gamma=0.577215664901532860;
		double expGamma=Math.exp(gamma);
		System.out.println("listPrimes="+initPrimes.size()+"  mu="+mu);
		long oldPrimeFactor=1;
		String listeFactorsMax=null;

		while((line=br.readLine())!=null)
		{
			if(!line.startsWith("#")&&!line.startsWith("//"))
			{
				line=line.trim();
				line=line.replaceAll(" ", ";");
				line=line.replaceAll(";;;", ";");
				line=line.replaceAll(";;", ";");
				String[] linespl=line.trim().split(";");
				
				int index=Integer.parseInt(linespl[0]);
				BigDecimal abundance=new BigDecimal(linespl[1]);
				double logcurrent=0;
				String typeNumber="";
				String listeFactors="";
				try {
					if(linespl[2].length()>0)
					{
						 logcurrent=Double.parseDouble(linespl[2])*Math.log(10);
						 typeNumber=linespl[3];
						 listeFactors=linespl[4];
					}
					else {
						logcurrent=Double.parseDouble(linespl[3])*Math.log(10);
						typeNumber=linespl[4];
						listeFactors=linespl[5];
					}
					
					if(typeNumber.equals("C"))
					{					
										double fn=abundance.doubleValue()/Math.log(logcurrent);
										
										double fnhyper=abundance.doubleValue()/Math.pow(Math.log(logcurrent), mu);
										
										
										String[] listeFacSpl=listeFactors.replace("{", "").replace("}", "").trim().split(",");
										
										long bigFactor=Long.parseLong(listeFacSpl[0]);
										
										
										int indexPrime=initPrimes.indexOf(bigFactor);
										long primeXfactor=initPrimes.get(indexPrime+1);
										
										double epsilon=Math.log(1+(1.0/(primeXfactor-0.1)))/Math.log(primeXfactor-0.1);
										
										if(fnhyper>maxRmLogLog&&index>19)
										{
											maxRmLogLog=fnhyper;
											maxM=index;
											listeFactorsMax=listeFactors;
											//System.out.println("---------pour mu ="+mu+"  "+index+"  "+fnhyper+"  "+listeFactors+"  "+logcurrent+"  "+fn+"  "+maxRmLogLog+"  "+typeNumber+"   true"+"   "+compteurXCA);
					                         
											if(typeNumber.equals("C"))
											compteurXA++;
										}
					}			
					
				}
				catch(Exception e)
				{
					System.out.println(line);
					e.printStackTrace();
				}

				//System.out.println(index+"   "+currentmagicsum+"   "+logcurrent+"   "+(rapportMagic)+"   "+currentRm+"   "+currentsuper);
			}
			
		}
		br.close();		
		
		System.out.println(mu+"  "+maxRmLogLog+"   "+maxRm+"  "+maxM+"  listeFactorsMax="+listeFactorsMax+"  compteurCA="+compteurCA+"  compteurepsilonInfLog="+compteurepsilonInfLog+"  compteurXCA="+compteurXCA);
        fos.write((mu+"  "+maxRmLogLog+"  "+maxM+"  listeFactorsMax="+listeFactorsMax+"  compteurCA="+compteurCA+"  compteurepsilonInfLog="+compteurepsilonInfLog+"  compteurXCA="+compteurXCA+'\n').getBytes());
		
	}
	
	
	
	
	
	

}