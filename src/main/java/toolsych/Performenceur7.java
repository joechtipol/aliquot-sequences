package toolsych;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Performenceur7 {

	public static void main(String[] args) throws IOException {

//		FileInputStream fis=new FileInputStream("c:/data/super-abund-list.txt");
		FileInputStream fis=new FileInputStream("c:/data/super-coloss-list.txt");

		InputStreamReader isr=new InputStreamReader(fis);
		BufferedReader br=new BufferedReader(isr);
		double somme=0;
		long compteur=0;
		String line=null;
		double maxRm=Double.MAX_VALUE;
		double maxRmLogLog=0;

		long maxM=0;
		double gamma=0.577215664901532860;
		double expGamma=Math.exp(gamma);
		List<Long> initPrimes=getPrimes(10000);
		List<Double> ProductMagicPrimes=getProductMagicPrimes(initPrimes, 1000);
		List<Double> SommeMagicPrimes=getSommeMagicPrimes(initPrimes, 1000);
        double oldLog=-1;
		while((line=br.readLine())!=null)
		{
			if(!line.startsWith("#"))
			{
				String[] linespl=line.split(" ");
				int index=Integer.parseInt(linespl[0]);
				BigInteger currentsuper=new BigInteger(linespl[1]);
				Map<BigInteger, Long> mapfacteurs=PrimeFactors.primefactormap(currentsuper);
				List<String> lstFacteurSorted=convertMapSorted(mapfacteurs);
				double logcurrent=logByFactors(mapfacteurs);
				BigInteger currentSigma=PrimeFactors.sigma(currentsuper);
				double ripportin=logcurrent*(1+(1.0/Math.log(logcurrent)));
				BigDecimal currentRm=new BigDecimal(currentSigma).divide(new BigDecimal(currentsuper),MathContext.DECIMAL128);
	            double currentmagicprod=ProductMagicPrimes.get((int) (logcurrent+1));
	            double currentmagicsum=SommeMagicPrimes.get((int) (logcurrent+1));
	            double sigmansurn=sigmaNsurNByFactors(mapfacteurs, 2);
	            String lastFacteur="";
	            long lastfacteurlg=1;
	            if(lstFacteurSorted.size()!=0)
	            {
	                lastFacteur=lstFacteurSorted.get(lstFacteurSorted.size()-1);
	                lastfacteurlg=Long.parseLong(lastFacteur.split(":")[0]);
	            }
	            double currentloglog=Math.log(logcurrent)*expGamma;
	            double magicDifference=currentRm.doubleValue()/Math.log(logcurrent);
	            //double rapportTomaximaise=(currentRm.doubleValue()/Math.pow(logcurrent, 0.15));
	            double rapportTomaximaise=0;
	            double rapportMagic=magicDifference;
	            if(rapportMagic>maxRmLogLog && index>19)
	            {
	            	if(oldLog==-1)
	            	{
	            		oldLog=logcurrent;
	            	}
	            	else {
	            		rapportTomaximaise=(logcurrent-oldLog)/Math.log(logcurrent);
	            		oldLog=logcurrent;
	            	}
	            	maxRmLogLog=rapportMagic;
	            	maxM=index;
	            	//if(rapportTomaximaise>1)
	            	{
						System.out.println(index+"   "+maxRmLogLog+"   "+logcurrent+"   "+rapportTomaximaise+"   "+rapportMagic+"   "+lstFacteurSorted);

	            	}

	            }

				//System.out.println(index+"   "+currentmagicsum+"   "+logcurrent+"   "+(rapportMagic)+"   "+currentRm+"   "+currentsuper);
			}
			
		}
		br.close();
		
		
		System.out.println(maxRmLogLog+"   "+maxRm+"  "+maxM);
		
		
	}
	
	
	
	
	public static double logByFactors(Map<BigInteger, Long> mapfacteurs)
	{ 
		double log=0;
		Iterator<BigInteger> e=mapfacteurs.keySet().iterator();
		while(e.hasNext())
		{
			BigInteger key=e.next();
			long value=mapfacteurs.get(key);
			log=log+(value*(Math.log(key.longValue())));
		}
		
		
		
		return log;
	}
	
	public static double sigmaNsurNByFactors(Map<BigInteger, Long> mapfacteurs,int degree)
	{ 
		double log=1;
		Iterator<BigInteger> e=mapfacteurs.keySet().iterator();
		while(e.hasNext())
		{
			BigInteger key=e.next();
			long value=mapfacteurs.get(key);
			if(value>=2)
			{
				log=log*getSommeDiviserurPrimeProper(key.longValue(),value);		
			}
		}
		
		
		
		return log;
	}
	
	
	
	public static double getSommeDiviserurPrimeProper(long prime,long power)
	{
		double resultat=0;
		for(int i=0;i<power+1;i++)
		{
			resultat=resultat+Math.pow(prime, -i);
		}
		
		
		return resultat;
	}
	
	
	public static List<Long> getPrimes(long XMAX)
	{
		List<Long> primes=new ArrayList<>();
		
		for(long i=2;i<=XMAX;i++)
		{
			long sprime=Performanceur4.sigmaprime(i);
			if(sprime==1)
			{
				primes.add(i);
			}

		}
		
		
		return primes;
		
	}
	public static List<Double> getSommeMagicPrimes(List<Long> primes,long seuil)
	{
		List<Double> primesProduct=new ArrayList<>();
		double sommeInv=0.0;
		long oldPrime=0;
		for(long i=0;i<primes.size();i++)
		{
			long currentprime=primes.get((int)i);
			for(long j=oldPrime;j<currentprime;j++)
			{
				primesProduct.add(sommeInv);
			}
			sommeInv=sommeInv+((1.0)/(currentprime));
			oldPrime=currentprime;
		}
		
		for(long j=oldPrime;j<seuil;j++)
		{
			primesProduct.add(sommeInv);
		}
		
		
		return primesProduct;
		
	}
	
	
	public static List<Double> getProductMagicPrimes(List<Long> primes,long seuil)
	{
		List<Double> primesProduct=new ArrayList<>();
		double product=1.0;
		long oldPrime=0;
		for(long i=0;i<primes.size();i++)
		{
			long currentprime=primes.get((int)i);
			for(long j=oldPrime;j<currentprime;j++)
			{
				primesProduct.add(product);
			}
			product=product*((1.0*currentprime)/(currentprime-1));
			oldPrime=currentprime;
		}
		
		for(long j=oldPrime;j<seuil;j++)
		{
			primesProduct.add(product);
		}
		
		
		return primesProduct;
		
	}
	
	
	
	
	public static List<String> convertMapSorted(Map<BigInteger, Long> facteurs)
	{
		ArrayList<String> result=new ArrayList<>();
		ArrayList<BigInteger> listeFacteurs=new ArrayList<>();		

		Iterator<BigInteger> e=facteurs.keySet().iterator();
		while(e.hasNext())
		{
			BigInteger key=e.next();
			listeFacteurs.add(key);
			
		}
		
		Collections.sort(listeFacteurs);
		for(int i=0;i<listeFacteurs.size();i++)
		{
			String current=listeFacteurs.get(i)+":"+facteurs.get(listeFacteurs.get(i));
			result.add(current);
		}
		
		
		
		
		
		
		
		
		return result;
		
		
		
	}
	
	

}