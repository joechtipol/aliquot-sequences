package toolsych.structure;

import java.util.Map;

public class PrimeFactorsData {
	long entier;
	
	Map<Long,Integer> primefactors;

	public long getEntier() {
		return entier;
	}

	public void setEntier(long entier) {
		this.entier = entier;
	}

	public Map<Long, Integer> getPrimefactors() {
		return primefactors;
	}

	public void setPrimefactors(Map<Long, Integer> primefactors) {
		this.primefactors = primefactors;
	}
	
	
	
	
	
	
}
