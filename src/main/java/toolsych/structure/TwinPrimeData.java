package toolsych.structure;

import java.util.List;

public class TwinPrimeData {
	
	long entier;
	
	List<Long> primefactors;
	
	double lambda;
	
	double lambdaR;
	
	int mobius;
	
	boolean isPrime=false;

	public boolean isPrime() {
		return isPrime;
	}

	public void setPrime(boolean isPrime) {
		this.isPrime = isPrime;
	}

	public int getMobius() {
		return mobius;
	}

	public void setMobius(int mobius) {
		this.mobius = mobius;
	}

	public long getEntier() {
		return entier;
	}

	public void setEntier(long entier) {
		this.entier = entier;
	}

	public List<Long> getPrimefactors() {
		return primefactors;
	}

	public void setPrimefactors(List<Long> primefactors) {
		this.primefactors = primefactors;
	}

	public double getLambda() {
		return lambda;
	}

	public void setLambda(double lambda) {
		this.lambda = lambda;
	}

	public double getLambdaR() {
		return lambdaR;
	}

	public void setLambdaR(double lambdaR) {
		this.lambdaR = lambdaR;
	}
	
	

}
