package toolsych;

import java.util.Map;

public class Performenceur6 {
	
	public static double[] primeslog= {1,1,2,3,3,3.75,3.75,4.375,4.375,4.375,4.375,4.8125,4.8125,
			5.2135,5.2135,5.2135,5.2135,5.5393,5.5393,5.8471,5.8471,5.8471,5.8471,6.1128,6.1128,6.1128,6.1128,6.1128,6.1128,6.3311,6.3311,6.5421,6.5421,6.5421,6.5421,
			6.5421,6.5421,6.72386,6.72386,6.72386,6.72386,6.891959,6.891959,7.056,7.056,7.056,7.056,7.2094,7.2094,7.2094,7.2094};

	public static void main(String[] args) {

		long Xmaxi=100000000;
		double somme=0;
		double sommeInv=0;
		double sommeGamaLog=0;
		double rapport=0;
		double hn=1;
		double maxiRapport=Double.MAX_VALUE;
		long maxIndex=0;
		Map<Long,Long> primeFactorization = null;
        double Hn=1;
		
		double gamma=0.577;
		double expGamma=Math.exp(gamma);
		 for (long i = 10000000; i < Xmaxi; i++) {
				long k=i;

			 long sprime=Performanceur4.sigmaprime(k);
		     long sigma=sprime+k;
		     double rapSigma=(1.0*sigma)/k;
		     int logint=new Float(Math.log(k)+1).intValue();

		     //int logint=new Float(Math.log(k)*(1+(1.0/Math.log(Math.log(k))))).intValue();
		     double differenceSignee=primeslog[logint]-rapSigma;
		     double difference=Math.abs(differenceSignee);
		     if(differenceSignee<0)
		     {
		    	 primeFactorization = Performanceur4.primeFactorsMap(k);
			        System.out.println("cococo  k="+k+" sigma="+sigma+"  difference="+difference+"  maxiRapport="+maxiRapport+"   maxIndex="+maxIndex+"  primeFactorization="+primeFactorization);

		     }

		     if(difference<maxiRapport)
		        {
		        	maxiRapport=difference;
		        	maxIndex=k;
		    		primeFactorization = Performanceur4.primeFactorsMap(k);
			        System.out.println("k="+k+" sigma="+sigma/(1.0*k)+"  difference="+difference+"  maxiRapport="+maxiRapport+"   maxIndex="+maxIndex+"  primeFactorization="+primeFactorization);

		        }
		     
		     if(i%10000==0)
			        System.out.println("k="+k+" sigma="+sigma/(1.0*k)+"  difference="+difference+"  maxiRapport="+maxiRapport+"   maxIndex="+maxIndex+"  primeFactorization="+primeFactorization+"  primeslog="+primeslog.length);

		 }
	}

}
