package toolsych;

import java.util.Map;

public class Performenceur5 {
	
public static void main(String[] args) {
		
		long Xmaxi=100000000;
		double somme=0;
		double sommeInv=0;
		double sommeGamaLog=0;
		double rapport=0;
		double hn=1;
		double maxiRapport=0;
		long maxIndex=0;
		Map<Long,Long> primeFactorization = null;
        double Hn=1;
		
		double gamma=0.577215664901532860;
		double expGamma=Math.exp(gamma);
		 for (long i = 2; i < Xmaxi; i++) {
			 Hn=Hn+(1.0/i);
		 }
		 hn=Hn;
		 for (long i = 361; i < Xmaxi; i++) {
				long k=i;
				long sprime=Performanceur4.sigmaprime(k);

		        long sigma=	sprime+k;

                double rapportTomaximaise=((1.0*sigma)/(k*Math.pow(Math.log(Math.log(k)), 1.35)));
		        
               // double rapportTomaximaise=((1.0*sigma)/(Math.pow(k, 1.01)));
		        
		        
		        if(rapportTomaximaise>maxiRapport)
		        {
		        	maxiRapport=rapportTomaximaise;
		        	maxIndex=k;
		    		primeFactorization = Performanceur4.primeFactorsMap(k);
			        System.out.println("    k="+k+" sigma="+sigma+"  rapportTomaximaise="+rapportTomaximaise+"  maxiRapport="+maxiRapport+"   maxIndex="+maxIndex+"  primeFactorization="+primeFactorization);

		        }

		       if(i%1000000==0)
			        System.out.println("k="+k+" sigma="+sigma+"  rapportTomaximaise="+rapportTomaximaise+"  maxiRapport="+maxiRapport+"   maxIndex="+maxIndex+"  primeFactorization="+primeFactorization);

		 }
		
	}
	
	
public static void mainhn2(String[] args) {
		
		long Xmaxi=100000000;
		double somme=0;
		double sommeInv=0;
		double sommeGamaLog=0;
		double rapport=0;
		double hn=1;
		double maxiRapport=Double.MAX_VALUE;
		long maxIndex=0;
		Map<Long,Long> primeFactorization = null;
        double Hn=1;
		
		double gamma=0.577;
		double expGamma=Math.exp(gamma);
		 for (long i = 2; i < Xmaxi; i++) {
			 Hn=Hn+(1.0/i);
		 }
		 hn=Hn;
		 for (long i = 1; i < Xmaxi; i++) {
				long k=Xmaxi-i;
				long sprime=Performanceur4.sigmaprime(k);

		        long sigma=	sprime+k;
		        sommeGamaLog=Math.log(Math.log(k))+gamma;
		        rapport=((1.0*sigma)/(k*Math.log(Math.log(k))));
		        double sommeHn=hn+(Math.exp(hn)*Math.log(hn));
		        

		        double rapportyFatal=(Math.exp(gamma)*Math.log(Math.log(k)))-((1.0*sigma)/k);
		        if(sprime==1)
		        {
		        	somme=somme+(1.0/(k-1));
		        	sommeInv=sommeInv+(1.0/(k));
		        }
		        if(rapportyFatal<maxiRapport)
		        {
		        	maxiRapport=rapportyFatal;
		        	maxIndex=k;
		    		primeFactorization = Performanceur4.primeFactorsMap(k);
			        System.out.println("k="+k+" sigma="+sigma+"  sommeHn="+sommeHn+"  rapportyFatal="+rapportyFatal+"  maxiRapport="+maxiRapport+"   maxIndex="+maxIndex+"  primeFactorization="+primeFactorization);

		        }
				hn=hn-(1.0/k);

		       // if(i%10000==0)
		 }
		
	}

	
	public static void mainHn(String[] args) {
		
		long Xmaxi=10000000;
		double somme=0;
		double sommeInv=0;
		double sommeGamaLog=0;
		double rapport=0;
		double hn=1;
		double maxiRapport=0;
		long maxIndex=0;
		Map<Long,Long> primeFactorization = null;
        double Hn=1;
		
		double gamma=0.577;
		double expGamma=Math.exp(gamma);
		 for (long i = 2; i < Xmaxi; i++) {
			 Hn=Hn+(1.0/i);
		 }
		 hn=Hn;
		 for (long i = 1; i < Xmaxi; i++) {
				long k=Xmaxi-i;
				long sprime=Performanceur4.sigmaprime(k);

		        long sigma=	sprime+k;
		        sommeGamaLog=Math.log(Math.log(k))+gamma;
		        rapport=((1.0*sigma)/(k*Math.log(Math.log(k))));
		        double sommeHn=hn+(Math.exp(hn)*Math.log(hn));
		        double rapportyFatal=sigma/sommeHn;
		        if(sprime==1)
		        {
		        	somme=somme+(1.0/(k-1));
		        	sommeInv=sommeInv+(1.0/(k));
		        }
		        if(rapportyFatal>maxiRapport)
		        {
		        	maxiRapport=rapportyFatal;
		        	maxIndex=k;
		    		primeFactorization = Performanceur4.primeFactorsMap(k);
			        System.out.println("k="+k+" sigma="+sigma+"  sommeHn="+sommeHn+"  rapportyFatal="+rapportyFatal+"  maxiRapport="+maxiRapport+"   maxIndex="+maxIndex+"  primeFactorization="+primeFactorization);

		        }
				hn=hn-(1.0/k);

		       // if(i%10000==0)
		 }
		
	}

		
		
	
	
	
	

	public static void mainExp(String[] args) {
		long Xmaxi=1000000000;
        double somme=0;
        double L=Math.exp(Math.log(Xmaxi)/Math.log(Math.log(Xmaxi)));

        for (long i = 2; i < Xmaxi/L; i++) {
			long k=i;
			long sprime=Performanceur4.sigmaprime(k);
			double rapport1=(1.0*(Xmaxi-k))/sprime;
			
			double rapport2=(1.0*(Xmaxi-k))/(k);

			double ssurn=(1.0*sprime)/k;
			double Llogk=L/Math.log(L);
			double rapport=(1.0*(Xmaxi-k))/k;
			
			double nbPrimes=rapport1/Math.log(rapport1);

			
			
			//double rapportBas=(1.0*(Xmaxi-k))/(2*k*Math.log(Math.log(k)));
			if(nbPrimes>Llogk+1)
			{
				somme=somme+(ssurn*(nbPrimes-Llogk));
				//somme=somme+(rapport/Math.log(rapportBas));
				//somme=somme+(1.0/(k*Math.log((1.0*Xmaxi)/k)));

			}
			
			//somme=somme+(1.0/(k*Math.log((Xmaxi-k)/i)));
			//if(i%10000==0)
			System.out.println("k="+k+"  sommme="+somme);

        }
		
	}

}
