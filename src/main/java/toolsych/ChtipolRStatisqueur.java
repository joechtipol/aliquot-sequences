package toolsych;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

public class ChtipolRStatisqueur {

	
	public static long NMAX=10000000;
	
	public static final BigInteger TOW=new BigInteger("2");

	
	public static void mainotherexp(String[] args) throws IOException {
        long Xmaxi=2000000000;
        double somme=0;
        double L=Math.exp(Math.log(Xmaxi)/Math.log(Math.log(Xmaxi)));

        for (long i = 2; i < Xmaxi/L; i++) {
			long k=2*i;
			somme=somme+(1.0/(k*Math.log((Xmaxi-k)/i)));
			if(i%10000==0)
			System.out.println("k="+k+"  sommme="+somme);

        }
		
		
	}

	
	
	public static void mainclcdiv(String[] args) throws IOException {
        long Xmaxi=2000000000;
        double PMAX=Math.pow(Xmaxi, 1.0/Math.log(Math.log(Xmaxi)));
        double L=Math.exp(Math.log(Xmaxi)/Math.log(Math.log(Xmaxi)));
        double PIPMAX=PMAX/Math.log(PMAX);
        double seuilMax=Xmaxi/L;
        double somme=0;
		for (long i = 2; i < seuilMax; i++) {
			long k=2*i;
			BigInteger currentBgi=new BigInteger(String.valueOf(k));
			long currentSigma=sum_of_divisors(currentBgi).longValue();
			//if(currentSigma>Xmaxi)
			{
				double denom=1.0*(Xmaxi-(k))/(k);
	            double valtotlog=Math.log(1.0*(Xmaxi-(k))/(2*k*Math.log(Math.log(k))));
	            if(PIPMAX<(denom/valtotlog))
	            somme=somme+(denom/valtotlog)-PIPMAX;
	            if(i%1000==0)
				System.out.println("currentBgi="+currentBgi+"  (denom/valtotlog)="+(denom/valtotlog)+"  PIPMAX="+PIPMAX+"  sommme="+somme);

			}
            

		}
		
		
		
		
		/*
		for (long i = 2; i < 100; i++) {
			BigInteger currentBgi=new BigInteger(String.valueOf(i));
			BigInteger currentSigma=sum_of_divisors(currentBgi);
			
			System.out.println("currentBgi="+currentBgi+"  currentSigma="+currentSigma);
		}*/

		
		
	}
	
	
	public static void main(String[] args) throws IOException {
		FileInputStream fis=new FileInputStream("c:/database/aliquot/mapyy_200000000_pairs.log");
		InputStreamReader isr=new InputStreamReader(fis);
		BufferedReader br=new BufferedReader(isr);
		double somme=0;
		long compteur=0;
		String line=null;
		double maxRm=0;
		long maxM=0;
		
ArrayList<Double> listeRm=new ArrayList<>();		
		double maxRapportm=0;
		long maxMrapport=0;
		while((line=br.readLine())!=null)
		{
			compteur++;
			String[] tab=line.split(";");
			
			long m=Long.parseLong(tab[0]);
			double rm=Double.parseDouble(tab[1]);
			if(m>0)
			{
				double rapport=rm/Math.log(m);
				if(rm>maxRm)
				{
					maxRm=rm;
					maxM=m;
				}
				
				if(rapport>maxRapportm)
				{
					maxRapportm=rapport;
					maxMrapport=m;
				}
				
				//if(m%3==2)
				somme=somme+(rm/m);
				listeRm.add(rm);
			}
			
			
			if(compteur%10000==0)
				System.out.println(compteur+"  : "+m+"  maxRapportm="+maxRapportm+"  maxMrapport="+maxMrapport+"  somme="+somme);

			
			
		}
       
		System.out.println(compteur+"  : "+"  somme="+somme);

		br.close();
		System.out.println(somme);
        int density=1000;
		 Collections.sort(listeRm);
		 double sommelisteRm=0;
	        for(int j=0;j<density;j++) {
	        	double currentMxRm=listeRm.get(listeRm.size()-j-1);
	        	sommelisteRm=sommelisteRm+currentMxRm;
	    		System.out.println(j+"  : "+"  listeRm[j]="+currentMxRm+"  sommelisteRm="+sommelisteRm);

	        }
	}

		
		
	
	public static void mainCLC(String[] args) throws IOException {
		long debut=System.currentTimeMillis();
        long compteur=0;
		HashMap<Long, Double> mapyy=new HashMap<Long, Double>();

		//HashMap<BigInteger, BigDecimal> mapyy=new HashMap<BigInteger, BigDecimal>();
		BigDecimal sommeRm=BigDecimal.ZERO;
		if(args.length>0)
		{
			NMAX=Long.parseLong(args[0]);
			
			System.out.println("NMAW recharged="+NMAX);
		}
		
		for(long i=1;i<=NMAX;i++)
		{
			BigInteger currentBgi=new BigInteger(String.valueOf(2*i));
			BigInteger currentSigma=sum_of_divisors(currentBgi);
			if((currentSigma.compareTo(new BigInteger(String.valueOf(NMAX)))== -1) && currentSigma.mod(new BigInteger("2")).equals(BigInteger.ZERO))
			{
				compteur++;
				//BigDecimal currentRm=new BigDecimal(currentSigma).divide(new BigDecimal(currentBgi),MathContext.DECIMAL128);
				BigDecimal currentRm=new BigDecimal(currentSigma).add(new BigDecimal(currentBgi)).divide(new BigDecimal(currentBgi),MathContext.DECIMAL128);

				BigDecimal currentLogRm=new BigDecimal(Math.log(currentRm.doubleValue()));
				
				
				
				if(mapyy.get(currentSigma.longValue())==null)
				{
					mapyy.put(currentSigma.longValue(), BigDecimal.ZERO.doubleValue());
				}
				double lastValue=mapyy.get(currentSigma.longValue());
				mapyy.put(currentSigma.longValue(),lastValue+currentLogRm.doubleValue());
				sommeRm=sommeRm.add(currentLogRm);
			}
			
			if(i%100000==0)
			System.out.println(currentBgi+"  : "+currentSigma+"  compteur="+compteur+"  sommeRm="+sommeRm+"  mappySize="+(1.0*mapyy.size())/i);

	
			 
			
		}
		
		
		/*
		for(long i=0;i<=NMAX/2;i++)
		{
			BigInteger currentBgi=new BigInteger(String.valueOf(2*i+1));
			BigInteger currentSigma=sum_of_divisors_carre(currentBgi);

			//BigInteger currentSigma=sum_of_divisors(currentBgi.pow(2));
			
			if((currentSigma.compareTo(new BigInteger(String.valueOf(NMAX)))== -1) && currentSigma.mod(new BigInteger("2")).equals(BigInteger.ZERO))
			{
				compteur++;
				BigDecimal currentRm=new BigDecimal(currentSigma).divide(new BigDecimal(currentBgi).pow(2),MathContext.DECIMAL128);
				if(mapyy.get(currentSigma.longValue())==null)
				{
					mapyy.put(currentSigma.longValue(), BigDecimal.ZERO.doubleValue());
				}
				double lastValue=mapyy.get(currentSigma.longValue());
				mapyy.put(currentSigma.longValue(),lastValue+(currentRm.doubleValue()));
				sommeRm=sommeRm.add(currentRm);

			}
			if(i%100000==0)
			System.out.println(currentBgi+"  : "+currentSigma+"  compteur="+compteur+"  sommeRm="+sommeRm+"  mappySize="+mapyy.size());
			

		}
		
		*/
		System.out.println(mapyy.size());
		
		
		//FileOutputStream fos=new FileOutputStream("c:/database/aliquot/mapyy_"+NMAX+"_pairs.log");
		
		
		FileOutputStream fos=new FileOutputStream("/root/aliquot-data/mapyy_"+NMAX+"_pairs.log");

		Iterator<Long> e=mapyy.keySet().iterator();
		 
		 while(e.hasNext())
		 {
			 long currentKey=e.next();
			 double currentValue=mapyy.get(currentKey);

		     fos.write((currentKey+";"+currentValue+'\n').getBytes());
		 }
		 
		fos.close();
		
		long timeFin=System.currentTimeMillis();
		long duree=timeFin-debut;
		System.out.println("duree="+duree);
		
	}
	
	
	public static void mainCarre(String[] args) throws IOException {
		long debut=System.currentTimeMillis();
        long compteur=0;
		HashMap<BigInteger, BigDecimal> mapyy=new HashMap<BigInteger, BigDecimal>();
		BigDecimal somme=new BigDecimal("0");
		
		for(long i=1;i<=NMAX;i++)
		{
			BigInteger currentBgi=new BigInteger(String.valueOf(i));

			
			//BigInteger currentSigma=sum_of_divisors(currentBgi.pow(2));
		BigInteger currentSigma=sum_of_divisors_carre(currentBgi);

			BigDecimal currentRm=new BigDecimal(currentSigma).divide(new BigDecimal(currentBgi).pow(2),MathContext.DECIMAL128);

			somme=somme.add(currentRm);
			
			if(i%100000==0)
			System.out.println(currentBgi+"  : "+currentSigma+"  somme="+somme+"  mappySize="+mapyy.size());
			

		}
		
		
		System.out.println(mapyy.size());
		
		
	}
	
	
	public static BigInteger sum_of_divisors(BigInteger number)
	{
		BigInteger sum = BigInteger.ONE;
		BigInteger n=number;

		for(BigInteger k = new BigInteger("2");k.multiply(k).compareTo(n)<=0; k=k.add(BigInteger.ONE))
		{
			BigInteger p =BigInteger.ONE;
			while(n.mod(k).equals(BigInteger.ZERO))
			{
				p = p.multiply(k).add(BigInteger.ONE);
				n=n.divide(k);
			}

			sum=sum.multiply(p);
		}

		if(n.compareTo(BigInteger.ONE)>0)
			sum =sum.multiply(n.add(BigInteger.ONE));

		return sum.subtract(number);
	}
	
	public static BigInteger sum_of_divisors_carre(BigInteger number)
	{
		BigInteger sum = BigInteger.ONE;
		BigInteger n=number;

		for(BigInteger k = new BigInteger("2");k.multiply(k).compareTo(n)<=0; k=k.add(BigInteger.ONE))
		{
			BigInteger p =BigInteger.ONE;
			int cptPow=0;
			while(n.mod(k).equals(BigInteger.ZERO))
			{
				cptPow++;
				p=p.add(k.pow(cptPow));
				cptPow++;		
				p=p.add(k.pow(cptPow));

				//p = p.multiply(k.multiply(k).add(k).add(BigInteger.ONE));
				n=n.divide(k);
			}

			sum=sum.multiply(p);
		}

		if(n.compareTo(BigInteger.ONE)>0)
			sum =sum.multiply(n.pow(2).add(n).add(BigInteger.ONE));

		return sum.subtract(number.pow(2));
	}

}
