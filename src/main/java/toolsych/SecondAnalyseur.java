package toolsych;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import toolsych.structure.PrimeFactorsData;

public class SecondAnalyseur {
	
	
	public static long N=10000000;
	public static long M=10000000;

	public static Map<Long,PrimeFactorsData> primefactorsinfosmapdata=new HashMap<>();
	
	public static PrimeFactorsData[] primefactorsinfostabdata=new PrimeFactorsData[(int) N];



	public static void main(String[] args) throws IOException {
		parsedataextend();
		writedataextendintelligent();
		//writedataextend();
	}
	
	public static List<Long> primeFactors(long number) {
	    long n = number;
	    List<Long> factors = new ArrayList<Long>();
	    for (long i = 2; i <= n; i++) {
	      while (n % i == 0) {
	        factors.add(i);
	        n /= i;
	      }
	    }
	    return factors;
	  }
	
	public static Map<Long,Integer> primeFactorsMap(long number) {
	    long n = number;
	    HashMap<Long, Integer> mapfactors=new HashMap<>();
	    List<Long> factors = new ArrayList<Long>();
	    for (long  i = 2; i <= n; i++) {
	      while (n % i == 0) {
	        factors.add(i);
	        if(mapfactors.get(i)==null)
	        {
	        	mapfactors.put(i, 0);
	        }
	        mapfactors.put(i,mapfactors.get(i)+1);
	        
	        n /= i;
	      }
	    }
	    return mapfactors;
	  }
	
	public static Map<Long,Integer> primeFactorsMapintelligent(long number) {
	    long n = number;
	    /*
	    if(primefactorsinfosmapdata.get(number)!=null)
	    {
	      return	primefactorsinfosmapdata.get(number).getPrimefactors();
	    }*/
	    
	    if(primefactorsinfostabdata[(int)number]!=null)
	    {
	      return	primefactorsinfostabdata[(int)number].getPrimefactors();
	    }
	    
	    
	    HashMap<Long, Integer> mapfactors=new HashMap<>();
	    for (long  i = 2; i <= n; i++) {
	      if (n % i == 0) {
	        long currentnumber=n/i;
	        if(currentnumber>1)
	        {
	        	//Map<Long, Integer> mapfactorscurrent=primefactorsinfosmapdata.get(currentnumber).getPrimefactors();
	        	Map<Long, Integer> mapfactorscurrent=primefactorsinfostabdata[(int)currentnumber].getPrimefactors();

	        	mapfactors.putAll(mapfactorscurrent);
	        	if(mapfactors.get(i)==null)
	        	{
	        		mapfactors.put(i,0);
	        	}
	        	mapfactors.put(i, mapfactors.get(i)+1);
	        }
	        else
	        {
	        	mapfactors.put(number, 1);
	        }
	        
	  	   PrimeFactorsData tpd=new PrimeFactorsData();
	 	   tpd.setEntier(number);
	 	   tpd.setPrimefactors(mapfactors);
		    //primefactorsinfosmapdata.put(number, tpd);
	 	  primefactorsinfostabdata[(int)number]=tpd;
		    return mapfactors;
	      }
	    }
	      //return	primefactorsinfosmapdata.get(number).getPrimefactors();
	      
	      return	primefactorsinfostabdata[(int)number].getPrimefactors();



	  }
	

	public static void writedataextendintelligent() throws IOException
	{
		
		FileOutputStream fos=new FileOutputStream("d:/primedataextend"+N+".txt");
		for(long i=2;i<N;i++)
		{
			Map<Long,Integer> primeFactorsiMap=primeFactorsMapintelligent(i);

			Set<Long> primeFactorsi=primeFactorsiMap.keySet();
			
			
			String display=i+";"+primeFactorsi.toString()+";"+primeFactorsiMap.toString()+'\n';
			
			fos.write(display.getBytes());
			
			if(i%10000==0)
			{
			  System.out.println("i="+i+" primeFactors="+primeFactorsi);

			}
			
		}
		
		fos.close();
				
	}
	
	public static void writedataextend() throws IOException
	{
		
		FileOutputStream fos=new FileOutputStream("d:/primedataextend"+N+".txt");
		for(long i=2;i<N;i++)
		{
			Map<Long,Integer> primeFactorsiMap=primeFactorsMap(i);

			Set<Long> primeFactorsi=primeFactorsiMap.keySet();
			
			
			String display=i+";"+primeFactorsi.toString()+";"+primeFactorsiMap.toString()+'\n';
			
			fos.write(display.getBytes());
			
			if(i%10000==0)
			{
			  System.out.println("i="+i+" primeFactors="+primeFactorsi);

			}
			
		}
		
		fos.close();
				
	}
	
	
	public static void parsedataextend() throws IOException
	{	
		primefactorsinfosmapdata=new HashMap<>();
		String thisLine=null;
		BufferedReader br = new BufferedReader(new FileReader("d:/primedataextend"+M+".txt"));
	       while ((thisLine = br.readLine()) != null) { 
	    	   String[] lineTab=thisLine.split(";");
	    	   long i=Long.valueOf(lineTab[0]);
	    	   String prmeFactorsStr=lineTab[2];

	    	   PrimeFactorsData tpd=new PrimeFactorsData();
	    	   tpd.setEntier(i);
	    	   tpd.setPrimefactors(arraysAsList(prmeFactorsStr)); 
	    	   
	    	  // primefactorsinfosmapdata.put(i, tpd);
	    	   primefactorsinfostabdata[(int)i]=tpd;
				if(i%10000==0)
				{
				  System.out.println("parsedataextend  i="+i);

				}
	    	   
	       }
	       
	       System.out.println(primefactorsinfosmapdata.size());

				
	}	
	
	
	public static Map<Long,Integer> arraysAsList(String doubleStr)
	{
		String[] tab=doubleStr.substring(1, doubleStr.length()-1).split(",");
		Map<Long,Integer> result=new HashMap<>();
		for(int i=0;i<tab.length;i++)
		{
			String currentKeyVal=tab[i].trim();
			String[] tabcurr=currentKeyVal.split("=");
			
			result.put(Long.valueOf(tabcurr[0].trim()),Integer.valueOf(tabcurr[1].trim()));
		}
		
		return result;
		
	}


}
