package toolsych.structure;

import java.util.Map;

public class AliquotNumber {
	
	public long sigma;
	
	protected Map<Long,Long> primeFactors;

	
	public long getSigma() {
		return sigma;
	}

	public void setSigma(long sigma) {
		this.sigma = sigma;
	}

	public Map<Long, Long> getPrimeFactors() {
		return primeFactors;
	}

	public void setPrimeFactors(Map<Long, Long> primeFactors) {
		this.primeFactors = primeFactors;
	}


}
