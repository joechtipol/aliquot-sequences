package toolsych;

import java.util.ArrayList;

public class Performenceur3 {
	
	public static final long NMAX=1000000;

	public static void main(String[] args) {
		double somme=0;
		for(long i=1;i<NMAX;i++)
		{
			ArrayList<Long> divlist=listDivisors(2*i);
			for(int j=0;j<divlist.size();j++)
			{
				long curr1=divlist.get(j);
				if(curr1%2==1)
				{
					for(int k=0;k<divlist.size();k++)
					{	     
						long curr2=divlist.get(k);
						//if(curr2<curr1 && curr2%2==1)
						{
							somme=somme+((1.0/(curr1*curr2)));
						}
					}
				}
				
			}	
			
			if(i%10000==0)
				System.out.println("i="+i+"   somme="+somme);
				
		}
		
		System.out.println("NMAX="+NMAX+"   somme="+somme);


	}
	
	
	static ArrayList<Long> listDivisors(long n) 
    { 
		ArrayList<Long> divlist=new ArrayList<>();
        // Note that this loop runs till square root 
        for (long i=1; i<=Math.sqrt(n); i++) 
        { 
            if (n%i==0) 
            { 
                // If divisors are equal, print only one 
                if (n/i == i) 
                	divlist.add(i);
       
                else // Otherwise print both 
                {
                	divlist.add(i); 
                	divlist.add(n/i); 
                }
            } 
        }
		return divlist; 
    } 

}
