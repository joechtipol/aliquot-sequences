package toolsych;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ErdosTesteur {
	
	
	public static double INVERSE_PI=1/Math.E;

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		long maxnumber=1000;
		ArrayList<Double> lista=new ArrayList<>();
		Map<Double, String> mapa=new HashMap<>();
		Map<Double, Double> mapa2=new HashMap<>();

		int valeurEntiere=(int)(1/INVERSE_PI);
		System.out.println("zm="+valeurEntiere);
		long compteur=0;
		for(int a=1;a<maxnumber;a++)
		{
			for(int v=1;v<(maxnumber*INVERSE_PI)+1;v++)
			{
				double z=a*INVERSE_PI-v;
				double zbarre=(1-a)*INVERSE_PI+v;
				if(z>0 && z<INVERSE_PI) {
					String av="("+a+",-"+v+")";
					String avbarre="("+(1-a)+","+v+")";
                    //if(v%valeurEntiere==2)
                    {
                    	lista.add(z);
    					lista.add(zbarre);
                    }
					
					mapa.put(z, av);
					mapa.put(zbarre, avbarre);

					mapa2.put(z, (1.0*a/v));
					mapa2.put(zbarre, (1.0*(a-1)/v));

					compteur++;
					System.out.println("compteur="+compteur+" (a,v)="+av+" and z="+z+" and zbarre="+zbarre+"  avbarre="+avbarre+"  a/v="+(1.0*a/v));
					//break;
				}
			}
		}
		
		Collections.sort(lista);
		compteur=0;
		for(int i=0;i<lista.size();i++)
		{
			compteur++;
			double zcur=lista.get(i);
			String avz=mapa.get(zcur);
			double asurv=mapa2.get(zcur);
			System.out.println("compteur="+compteur+" (a,v)="+avz+" and z="+zcur+" and asurv="+asurv);

			
			
		}
		
		
		
		
		
		
		
		
	}

}
