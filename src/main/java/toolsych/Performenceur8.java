package toolsych;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Performenceur8 {

	public static void main(String[] args) throws IOException {

//		FileInputStream fis=new FileInputStream("c:/data/super-abund-list.txt");
		FileInputStream fis=new FileInputStream("c:/data/SAN_1000000.txt");
		
		
		//FileOutputStream fos=new FileOutputStream("c:/data/SAN_modified_mu_ca_1000000.txt");

		InputStreamReader isr=new InputStreamReader(fis);
		BufferedReader br=new BufferedReader(isr);
		double somme=0;
		long compteur=0;
		String line=null;
		double maxRm=Double.MAX_VALUE;
		double maxRmLogLog=0;
		double minHyperRapport=Double.MAX_VALUE;

		long maxM=0;
		long compteurCA=0;
		long compteurXA=0;
		long compteurXCA=0;
		long compteurXSA=0;


		long compteurepsilonInfLog=0;
		long compteurepsilonSupLog=0;

		double gamma=0.577215664901532860;
		double expGamma=Math.exp(gamma);
		List<Long> initPrimes=getPrimes(1000000);
		System.out.println("listPrimes="+initPrimes.size());
		long oldPrimeFactor=1;

		while((line=br.readLine())!=null)
		{
			if(!line.startsWith("#")&&!line.startsWith("//"))
			{
				line=line.trim();
				line=line.replaceAll(" ", ";");
				line=line.replaceAll(";;;", ";");
				line=line.replaceAll(";;", ";");
				String[] linespl=line.trim().split(";");
				
				int index=Integer.parseInt(linespl[0]);
				BigDecimal abundance=new BigDecimal(linespl[1]);
				double logcurrent=0;
				double mu=1.00159;
				String typeNumber="";
				String listeFactors="";
				try {
					if(linespl[2].length()>0)
					{
						 logcurrent=Double.parseDouble(linespl[2])*Math.log(10);
						 typeNumber=linespl[3];
						 listeFactors=linespl[4];
					}
					else {
						logcurrent=Double.parseDouble(linespl[3])*Math.log(10);
						typeNumber=linespl[4];
						listeFactors=linespl[5];
					}
					
					double fn=abundance.doubleValue()/Math.log(logcurrent);
					
					double fnhyper=abundance.doubleValue()/Math.pow(Math.log(logcurrent), mu);
					
					
					String[] listeFacSpl=listeFactors.replace("{", "").replace("}", "").trim().split(",");
					
					long bigFactor=Long.parseLong(listeFacSpl[0]);
					
					
					int indexPrime=initPrimes.indexOf(bigFactor);
					long primeXfactor=initPrimes.get(indexPrime+1);
					long primeYfactor=initPrimes.get(indexPrime+2);

					double epsilon=Math.log(1+(1.0/(primeXfactor)))/Math.log(primeXfactor);
					double epsilon2=Math.log(1+(1.0/(bigFactor)))/Math.log(primeXfactor-0.1);

					
						if(fn>=maxRmLogLog&&index>19)
						{
						maxRmLogLog=fn;
						maxM=index;
						compteurXA++;
						}

						if(typeNumber.equals("C"))
						{

							
							double logMinvdbl=1.0/(logcurrent*Math.log(logcurrent));
							
							
							double uplusone=(epsilon/logMinvdbl);
							String toFileStr=index+";"+  uplusone+";"+abundance+";"+logcurrent+";"+listeFactors;

							//if(fn==maxRmLogLog)
							{

								 fnhyper=abundance.doubleValue()/Math.pow(Math.log(logcurrent), uplusone);
								 //double hyperrapport=(Math.pow(Math.log(logcurrent), uplusone-1)-1)*Math.pow(Math.log(logcurrent), 2);
								 double hyperrapport=(epsilon+epsilon2)/2;
								 minHyperRapport=1.0/(logcurrent*Math.log(logcurrent));
							String listN0=calculerN0(logMinvdbl, logcurrent+0.0001, initPrimes);

		                        if(listN0.equals(listeFactors))
		                        {
									compteurXCA++;
		    						System.out.println("------"+index+"  "+minHyperRapport+"  "+listeFactors+"  "+primeXfactor+"  "+logcurrent+"  "+fn+"  "+maxRmLogLog+"  "+typeNumber+"   true"+"   "+compteurXCA);

		                        }
		                        else
		                        {
		    						System.out.println(index+"  "+minHyperRapport+"  "+listN0+"  "+listeFactors+"  "+primeXfactor+"  "+logcurrent+"  "+fn+"  "+maxRmLogLog+"  "+typeNumber+"   true"+"   "+compteurXCA);

		                        }

							}


							
							//String listN0=calculerN0(logMinvdbl, logcurrent+0.0001, initPrimes);
							//String toFileStr=epsilon+" "+logMinvdbl+"  "+  index+"  "+abundance+"  "+fn+"  "+maxRmLogLog+  "  "+bigFactor+"  "+primeXfactor+"  "+logcurrent+"  "+typeNumber+"  "+listeFactors;
                            
							
							/*if(primeYfactor<(logcurrent-1))
							{
								String mylistXA=calculerN0ListXA(listeFacSpl, initPrimes);
								if(!mylistXA.equals(listeFactors))
								{
									compteurXCA++;
									  fnhyper=abundance.doubleValue()/Math.pow(Math.log(logcurrent), uplusone);
		    							System.out.println(index+"  "+bigFactor+"  "+primeYfactor+"  "+listeFactors+"  "+mylistXA+"  "+logcurrent+"  "+fn+"  "+maxRmLogLog+"  "+typeNumber+"   true"+"   "+compteurXCA);

								}
							  
							}*/
							
						}
						else
						{
							compteurXSA++;
						}

					
						/*	
							if(logMinvdbl<epsilon)
							{			compteurepsilonSupLog++;	
							System.out.println(toFileStr);

							}
							else
							{
								compteurepsilonInfLog++;	
								//if(primeXfactor<logcurrent)
								System.err.println(toFileStr);

							}
						}*/
					}
					
				catch(Exception e)
				{
					System.out.println(line);
					e.printStackTrace();
				}

				//System.out.println(index+"   "+currentmagicsum+"   "+logcurrent+"   "+(rapportMagic)+"   "+currentRm+"   "+currentsuper);
			}
			
		}
		br.close();
		//fos.close();
		
		
		System.out.println(maxRmLogLog+"   "+maxRm+"  "+maxM+"  compteurXA="+compteurXA+"  compteurXCA="+compteurXCA+"  compteurXSA="+compteurXSA);
		
		
	}
	
	
	
	
	public static double logByFactors(Map<BigInteger, Long> mapfacteurs)
	{ 
		double log=0;
		Iterator<BigInteger> e=mapfacteurs.keySet().iterator();
		while(e.hasNext())
		{
			BigInteger key=e.next();
			long value=mapfacteurs.get(key);
			log=log+(value*(Math.log(key.longValue())));
		}
		
		
		
		return log;
	}
	
	public static double sigmaNsurNByFactors(Map<BigInteger, Long> mapfacteurs,int degree)
	{ 
		double log=1;
		Iterator<BigInteger> e=mapfacteurs.keySet().iterator();
		while(e.hasNext())
		{
			BigInteger key=e.next();
			long value=mapfacteurs.get(key);
			if(value>=2)
			{
				log=log*getSommeDiviserurPrimeProper(key.longValue(),value);		
			}
		}
		
		
		
		return log;
	}
	
	
	
	public static double getSommeDiviserurPrimeProper(long prime,long power)
	{
		double resultat=0;
		for(int i=0;i<power+1;i++)
		{
			resultat=resultat+Math.pow(prime, -i);
		}
		
		
		return resultat;
	}
	
	
	public static List<Long> getPrimes(long XMAX)
	{
		List<Long> primes=new ArrayList<>();
		
		for(long i=2;i<=XMAX;i++)
		{
			long sprime=Performanceur4.sigmaprime(i);
			if(sprime==1)
			{
				primes.add(i);
			}

		}
		
		
		return primes;
		
	}
	public static List<Double> getSommeMagicPrimes(List<Long> primes,long seuil)
	{
		List<Double> primesProduct=new ArrayList<>();
		double sommeInv=0.0;
		long oldPrime=0;
		for(long i=0;i<primes.size();i++)
		{
			long currentprime=primes.get((int)i);
			for(long j=oldPrime;j<currentprime;j++)
			{
				primesProduct.add(sommeInv);
			}
			sommeInv=sommeInv+((1.0)/(currentprime));
			oldPrime=currentprime;
		}
		
		for(long j=oldPrime;j<seuil;j++)
		{
			primesProduct.add(sommeInv);
		}
		
		
		return primesProduct;
		
	}
	
	
	
	public static List<Double> getProductMagicPrimes(List<Long> primes,long seuil)
	{
		List<Double> primesProduct=new ArrayList<>();
		double product=1.0;
		long oldPrime=0;
		for(long i=0;i<primes.size();i++)
		{
			long currentprime=primes.get((int)i);
			for(long j=oldPrime;j<currentprime;j++)
			{
				primesProduct.add(product);
			}
			product=product*((1.0*currentprime)/(currentprime-1));
			oldPrime=currentprime;
		}
		
		for(long j=oldPrime;j<seuil;j++)
		{
			primesProduct.add(product);
		}
		
		
		return primesProduct;
		
	}
	
	
	
	
	public static List<String> convertMapSorted(Map<BigInteger, Long> facteurs)
	{
		ArrayList<String> result=new ArrayList<>();
		ArrayList<BigInteger> listeFacteurs=new ArrayList<>();		

		Iterator<BigInteger> e=facteurs.keySet().iterator();
		while(e.hasNext())
		{
			BigInteger key=e.next();
			listeFacteurs.add(key);
			
		}
		
		Collections.sort(listeFacteurs);
		for(int i=0;i<listeFacteurs.size();i++)
		{
			String current=listeFacteurs.get(i)+":"+facteurs.get(listeFacteurs.get(i));
			result.add(current);
		}
		
		
		
		
		
		
		
		
		return result;
		
		
		
	}
	
	
	
	public static String calculerN0(double epsilonzero,double x,List<Long> primes)
	{
		
		String listFactors="{";
		int exp2=0;
		{
			long currentPrime=2;
			
				double denominateur=Math.pow(currentPrime, epsilonzero+1)-1;
				double sousnominateur=Math.pow(currentPrime, epsilonzero)-1;

				double epsilon=Math.log((denominateur/sousnominateur))/Math.log(currentPrime);
				
				exp2=((int) epsilon) -1;
		}
		
		
		if(exp2>0)
		{
			long [] tabPrimes=new long[exp2];
			
			for(int i=0;i<primes.size();i++)
			{
				long currentPrime=primes.get(i);
				//if(currentPrime<x)
				{
					double denominateur=Math.pow(currentPrime, epsilonzero+1)-1;
					double sousnominateur=Math.pow(currentPrime, epsilonzero)-1;

					double epsilon=Math.log((denominateur/sousnominateur))/Math.log(currentPrime);
					
					int facteureps=((int) epsilon) -1;
					if(facteureps>0)
					{
						//listFactors.add(currentPrime+":"+facteureps);
						tabPrimes[exp2-facteureps]=currentPrime;

					}
					else
					{
						break;
					}
				}
				
				/*else {
					break;
				}*/
				

			}	
			
			for(int i=0;i<tabPrimes.length;i++)
			{
				if(i==0)
				{
					listFactors=listFactors+tabPrimes[tabPrimes.length-i-1];
				}
				else
				{
					listFactors=listFactors+","+tabPrimes[tabPrimes.length-i-1];

				}
			}
		}
		
		
		

		
		listFactors=listFactors+"}";
		
		
		
      return listFactors;
	}
	
	
	public static List<Long> calculerN0List(double epsilonzero,double x,List<Long> primes)
	{
		
		ArrayList<Long> listFactors=new ArrayList<>();
		int exp2=0;
		{
			long currentPrime=2;
			
				double denominateur=Math.pow(currentPrime, epsilonzero+1)-1;
				double sousnominateur=Math.pow(currentPrime, epsilonzero)-1;

				double epsilon=Math.log((denominateur/sousnominateur))/Math.log(currentPrime);
				
				exp2=((int) epsilon) -1;
		}
		
		long [] tabPrimes=new long[exp2];
		
		for(int i=0;i<primes.size();i++)
		{
			long currentPrime=primes.get(i);
			if(currentPrime<x)
			{
				double denominateur=Math.pow(currentPrime, epsilonzero+1)-1;
				double sousnominateur=Math.pow(currentPrime, epsilonzero)-1;

				double epsilon=Math.log((denominateur/sousnominateur))/Math.log(currentPrime);
				
				int facteureps=((int) epsilon) -1;
				if(facteureps>0)
				{
					//listFactors.add(currentPrime+":"+facteureps);
					tabPrimes[exp2-facteureps]=currentPrime;

				}
			}
			
			else {
				break;
			}
			

		}
		
		
		for(int i=0;i<tabPrimes.length;i++)
		{
			listFactors.add(tabPrimes[tabPrimes.length-i-1]);
		}
		
		
		
		
      return listFactors;
	}
	
	
	
	
	public static String calculerN0ListXA(String[] listeFacSpl,List<Long> primes)
	{
		
		long exp2=listeFacSpl.length;
		String listFactors="{";

		
		long [] tabPrimes=new long[(int)exp2];
		long pMax=Long.parseLong(listeFacSpl[0]);
		
		for(int i=0;i<primes.size();i++)
		{
			long currentPrime=primes.get(i);
			
			
			int alphaqp=(int) (Math.log(1+((currentPrime-1.0)*((pMax*Math.log(pMax))/(currentPrime*Math.log(currentPrime)))))/Math.log(currentPrime));
			
			
			if(alphaqp>0)
			{
				tabPrimes[(int)exp2-alphaqp]=currentPrime;
			}
			else {
				break;
			}
			

		}
		
		
		for(int i=0;i<tabPrimes.length;i++)
		{
			if(i==0)
			{
				listFactors=listFactors+tabPrimes[tabPrimes.length-i-1];
			}
			else
			{
				listFactors=listFactors+","+tabPrimes[tabPrimes.length-i-1];

			}
		}
		
		listFactors=listFactors+"}";

		
		
      return listFactors;
	}
	
	
	
	

}