package toolsych;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileStatisqueur {

		public static void main5e7(String[] args) throws IOException
		{
			String thisLine=null;
	   int compteur=0;
			BufferedReader br = new BufferedReader(new FileReader("c:/database/aliquot/valitttop_5e7"));
			double somme=0;
			double sommenBImage=0;

			long maxAntecedantsPairs=0;
			long maxNbAntecedantsPairs=0;

			double maxAntecedantsPairsRapport=0;

		       while ((thisLine = br.readLine()) != null) { 
		    	  // System.out.println(thisLine);
	    		   String argsparsedstr=thisLine.replace("{", "");
	    		   argsparsedstr=argsparsedstr.replace("}", "");
	    		   argsparsedstr=argsparsedstr.trim();
		    	   String[] argsparsed=argsparsedstr.split(",");
		    	   for(int i=0;i<argsparsed.length;i++)
		    	   {

		    		   if(argsparsed[i].trim().length()>0)
		    		   {
			    		   compteur++;
			    		   long nbAntecedants=Long.parseLong(argsparsed[i].trim());
			    		   if(compteur%3==0)
			    		   {
				    		   sommenBImage=sommenBImage+nbAntecedants;

			    			   double rapportcurr=nbAntecedants/Math.log(compteur);
				    		   if(rapportcurr>maxAntecedantsPairsRapport)
				    		   {
				    			   maxAntecedantsPairsRapport=rapportcurr;
				    			   maxAntecedantsPairs=compteur;
				    			   maxNbAntecedantsPairs=nbAntecedants;
				    		   }
			    		   }
			    		  
			    		  
	                       if(compteur%100000==0)
	                    	   // if(compteur<1000)
			    		   System.out.println("i="+compteur+" nbAntecedants="+ nbAntecedants+"  maxAntecedantsPairs="+maxAntecedantsPairs+"  maxNbAntecedantsPairs="+maxNbAntecedantsPairs+"  sommenBImage="+sommenBImage);

		    		   }
		    	   }

		       }
		}
		
		
		public static void main(String[] args) throws IOException
		{
			String thisLine=null;
	   long compteur=0;
	   long cptouchable=0;
	   
	   FileInputStream inputStream = new FileInputStream("c:/database/aliquot/antecpairnomb_5e8");
	   
	   
	   char c='\n';
	   String countAntecStr="";
	   long nbAntec=0;
	   long nbAntecTotal=0;

	   double sommeIncha=0;
	   int idx=-1;
	   byte[] buffer=new byte[8192];
	   long maxAntecedantsPairs=0;
		long maxNbAntecedantsPairs=0;

		double maxAntecedantsPairsRapport=0;
	   
	   while((idx=inputStream.read(buffer))!=-1)
	   {
		   for(int i=0;i<buffer.length;i++)
		   {
			    c=(char)buffer[i];
			    if(c==-1)
			    	break;
			   if(c==' '|| c=='[' || c==']' || c=='\n') {
				   
			   }
			   else {
				   if(c==',')
				   {
					   compteur+=2;
					   nbAntec=Long.parseLong(countAntecStr);
					   nbAntecTotal=nbAntecTotal+nbAntec;
					   if(nbAntec==0)
					   {
						   cptouchable++;
					   }
					   else
					   {
						   double rapportcurr=nbAntec/Math.log(compteur);
			    		   if(rapportcurr>maxAntecedantsPairsRapport)
			    		   {
			    			   maxAntecedantsPairsRapport=rapportcurr;
			    			   maxAntecedantsPairs=compteur;
			    			   maxNbAntecedantsPairs=nbAntec;
			    		   }
			    		   sommeIncha=sommeIncha+(1.0*nbAntec);
					   }
					  
					   
		    		   if(compteur%100000==0)
					   {
			    		   System.out.println("i="+compteur+"   sommeIncha="+sommeIncha+" nbAntec="+ nbAntec+"  maxAntecedantsPairs="+maxAntecedantsPairs+"  maxNbAntecedantsPairs="+maxNbAntecedantsPairs+"  maxAntecedantsPairsRapport="+maxAntecedantsPairsRapport);

					   }
					   countAntecStr="";
				   }
				   else
				   {
					   countAntecStr=countAntecStr+c;
				   }
				   
				   
			   }
			   
			   
		   }
	   }
	   
	   System.out.println("cptouchable="+cptouchable+"  nbAntecTotal="+nbAntecTotal+"  compteur="+compteur+"   sommeIncha="+sommeIncha);
	   
		}
		
		
		
	}

