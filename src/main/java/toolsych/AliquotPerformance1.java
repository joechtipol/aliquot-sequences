package toolsych;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AliquotPerformance1 {

	public static final long N_MAX = 100000000;

	
	//main s2
	public static void mains2(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("c:/database/aliquot/100000000_sigmas.txt"));
		FileOutputStream fos=new FileOutputStream("c:/database/aliquot/"+N_MAX+"_sigmas_2_db.sql");

        Map<Long, Long> sigmasTab=new HashMap<>();
		String line = br.readLine();
        int compteur=0;
		while (line != null) {
			
			String[] tab=line.split(";");
			long current=Long.parseLong(tab[0].trim());
			long currentSig=Long.parseLong(tab[1].trim());
			long sigma2=sigmaprime(currentSig);
			//sigmasTab.put(current, currentSig);
			String insertsql="INSERT INTO test_sigma2 (numbern, sprime2) VALUES ("+current+","+sigma2+");";
			fos.write((insertsql+'\n').getBytes());
			//System.out.println(insertsql);
			//if(sigma2<N_MAX)
			if(current%10000==0)
			System.out.println("i="+current+"  currentSig="+currentSig+"  sigma2="+sigma2+"  compteur="+compteur++);
			line = br.readLine();
		}
		
		System.out.println(sigmasTab.size());
		br.close();
		fos.close();

	}	
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("c:/database/aliquot/100000000_sigmas.txt"));
		FileOutputStream fos=new FileOutputStream("c:/database/aliquot/"+N_MAX+"_sigmas_db_10.sql");

        Map<Long, Long> sigmasTab=new HashMap<>();
		String line = br.readLine();

		while (line != null) {
			
			String[] tab=line.split(";");
			long current=Long.parseLong(tab[0].trim());
			long currentSig=Long.parseLong(tab[1].trim());
			//sigmasTab.put(current, currentSig);
			//if(current>1122645&& current<10000000)
			//if(current>9999999 && current<20000000)
			if(current>89999999 && current<100000000)
			{
			String insertsql="INSERT INTO test_sigma (number, sprime) VALUES ("+current+","+currentSig+");";
			fos.write((insertsql+'\n').getBytes());
			}
			//System.out.println(insertsql);
			if(current%10000==0)
			System.out.println("i="+current+"  currentSig="+currentSig);
			line = br.readLine();
		}
		
		System.out.println(sigmasTab.size());
		br.close();
		fos.close();

	}
	
	public static void mainoldone(String[] args) {
		long timeDebut = System.currentTimeMillis();

		long compteur = 0;
		double somme = 0;
		for (long i = 2; i < N_MAX; i++) {
			Map<Long, Long> primeFactorization = primeFactorsMap(i);
			if(i%10000==0)
			System.out.println("i=" + i + "  primeFactorization=" + primeFactorization);
		}

		long timeFin = System.currentTimeMillis();
		long duree = timeFin - timeDebut;
		System.out.println("duree=" + duree);

	}

	public static void mainregina(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("c:/database/aliquot/regina_file"));

		String line = br.readLine();

		while (line != null) {
			System.out.println(line);
			line = br.readLine();
		}
		br.close();

	}

	public static Map<Long,Long> primeFactorsMap(long numbers) {
	    long n = numbers;
	    List<Long> factors = new ArrayList<Long>();
	    
	    Map<Long,Long> factorsMap = new HashMap<Long, Long>();

	    for (long i = 2; i <= n / i; i++) {
	      while (n % i == 0) {
	        factors.add(i);
	        
	        long compteur=0;
		      if(factorsMap.get(i)!=null)
		      {
		    	  compteur=factorsMap.get(i);
		      }
		      factorsMap.put(i, compteur+1);
	        
	        n /= i;
	      }
	    }
	    if (n > 1) {
	      factors.add(n);
	      long compteur=0;
	      if(factorsMap.get(n)!=null)
	      {
	    	  compteur=factorsMap.get(n);
	      }
	      factorsMap.put(n, compteur+1);
	      
	      
	    }
	    return factorsMap;
	  }

	
	public static long sigmaprime(long N)
	{
		Map<Long,Long> primeFactorization = primeFactorsMap(N);
		long total = 1;
		for (long p:primeFactorization.keySet()) {
		    long factor = 1;
		    for (long i=0;i<primeFactorization.get(p);i++) {
		        factor*=p;
		        factor+=1;
		    }
		    total*=factor;
		}
		return total-N;
		
	}

}
